from django.db import models
from django.db.models import Sum
from django.utils.translation import gettext as _
from django.utils.timezone import now
from django.conf import settings
from decimal import Decimal
from datetime import timedelta

from users.models import User
from .choices import GATEWAY_TYPES
from platform_management.convenience import get_config


class Gateway(models.Model):
    name = models.CharField(max_length=200, blank=False, unique=True)
    display_name = models.CharField(max_length=200, blank=False)
    demo = models.BooleanField(_("Is running in sandbox?"), default=True)
    store_id = models.CharField(max_length=200, blank=False, help_text="Live: verticalinnovationslive, Sandbox: testbox")
    store_password = models.CharField(max_length=200, blank=False, help_text="Live: 5EC63DB38D10D77683, Sandbox: qwerty")
    demo_session_url = models.CharField(max_length=200, blank=True)
    demo_validation_url = models.CharField(max_length=200, blank=True)
    live_session_url = models.CharField(max_length=200, blank=True)
    live_validation_url = models.CharField(max_length=200, blank=True)
    active = models.BooleanField(default=True)
    bank_transaction_fee_percentage = models.DecimalField(_("Bank transaction fee (in percentage)"), max_digits=5, decimal_places=2, default=2.5)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        if self.demo:
            demo_title = "SANDBOX"
        else:
            demo_title = "PRODUCTION"
        return self.display_name + " | " + demo_title


class Invoice(models.Model):
    id = models.CharField(max_length=200, blank=False, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='invoice_user', editable=False)
    discount_applied = models.BooleanField(_("Company Discount Applied"), default=False)
    discount_company = models.CharField(max_length=150, blank=True)
    discount_rate = models.DecimalField(_("Company Discount Rate"), max_digits=5, decimal_places=2, help_text='in percentage (%)', default=0)
    area_discount_applied = models.BooleanField(_("Area Discount Applied"), default=False)
    area_discount_rate = models.DecimalField(_("Area Discount Rate"), max_digits=5, decimal_places=2, help_text='in percentage (%)', default=0)
    ru_name = models.CharField(max_length=250, blank=True)
    ru_tag = models.CharField(max_length=250, blank=True)
    ru_address = models.TextField(_("Address"), default="")
    ru_area = models.CharField(max_length=250, blank=True)
    ru_zone = models.CharField(max_length=250, blank=True)
    ru_lat = models.FloatField(_("Latitude"), default=0.0)
    ru_lon = models.FloatField(_("Longitude"), default=0.0)
    bank_transaction_fee_percentage = models.DecimalField(_("Bank transaction fee (in percentage)"), max_digits=5, decimal_places=2, default=0.0)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Invoices"

    def __str__(self):
        return str(self.id)

    def base_total(self):
        items = CartItem.objects.filter(invoice=self)
        total = 0
        if items.exists():
            for item in items:
                total += item.total_price()
        return round(total, 2)

    def discount_amount(self):
        d_amount = Decimal(0.0)
        if self.area_discount_applied:
            d_amount = Decimal(self.base_total()) * (self.area_discount_rate / Decimal(100))
        if self.discount_applied:
            d_amount = Decimal(self.base_total()) * (self.discount_rate / Decimal(100))
        return round(d_amount, 2)

    def tax_total(self):
        total = self.base_total()
        total = total - self.discount_amount()
        tax_amount = Decimal(total) * (self.bank_transaction_fee_percentage / Decimal(100))
        total += round(tax_amount, 2)
        return total

    def cart_items(self):
        return CartItem.objects.filter(invoice=self)

    def total_products(self):
        return self.cart_items().aggregate(total_quantity=Sum('quantity'))['total_quantity']

    def total_dispensed(self):
        return self.cart_items().filter(dispensed=True).aggregate(total_quantity=Sum('quantity'))['total_quantity']

    def dispensed(self):
        not_dispensed = self.cart_items().filter(dispensed=False).exists()
        return not not_dispensed
    dispensed.boolean = True

    def is_paid(self):
        return Transaction.objects.filter(invoice=self, status="PAID").exists()
    is_paid.boolean = True

    def transactions(self):
        return Transaction.objects.filter(invoice=self)

    def expiry_date(self):
        paid_transactions = Transaction.objects.filter(invoice=self, status='PAID')
        if paid_transactions.exists():
            transaction = paid_transactions.order_by('-tran_date').first()
            hours = get_config().cart_timeout if get_config() is not None else settings.CART_HOLDOUT_HOURS
            return transaction.tran_date + timedelta(hours=hours)
        else:
            return None

    def dispensable(self):
        if self.expiry_date():
            if self.expiry_date() > now():
                return True
            return False
        return False
    dispensable.boolean = True


class CartItem(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='cart_item_user', editable=False)
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, related_name='cart_invoice', editable=False, null=True, blank=True)
    inventory_type = models.CharField(max_length=250, blank=True)
    product_brand = models.CharField(max_length=250, blank=True)
    product_class = models.CharField(max_length=250, blank=True)
    product_size = models.CharField(max_length=250, blank=True)
    unit_price = models.DecimalField(max_digits=9, decimal_places=2, default=0.0)
    mag_no = models.PositiveIntegerField(default=0)
    quantity = models.PositiveIntegerField(_("Quantity"), default=1)
    dispensed = models.BooleanField(_("Dispensed"), default=False)
    dispense_date = models.DateTimeField(auto_now=False, auto_now_add=False, default=now)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Cart Items"

    def __str__(self):
        return self.invoice.id + ' | ' +str(self.id)

    def total_price(self):
        return self.unit_price * self.quantity


class Transaction(models.Model):
    id = models.CharField(max_length=200, blank=False, primary_key=True)
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, related_name='transaction_invoice', editable=False, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='transaction_user', editable=False)
    gateway_type = models.IntegerField(_("Gateway Type"), choices=GATEWAY_TYPES, default=1)
    rfid_tag= models.CharField(max_length=250, blank=True)
    ru_tag = models.CharField(max_length=250, blank=True)
    status = models.CharField(_("Payment Status"), max_length=200, blank=False, default="PENDING")
    dispensed = models.BooleanField(_("Dispensed"), default=False)
    session_key = models.CharField(max_length=200, blank=True)
    validation_id = models.CharField(max_length=200, blank=True)
    amount = models.CharField(max_length=200, blank=True)
    store_amount = models.CharField(max_length=200, blank=True)
    currency = models.CharField(max_length=50, blank=True)
    card_type = models.CharField(max_length=200, blank=True)
    card_issuer = models.CharField(max_length=200, blank=True)
    card_brand = models.CharField(max_length=200, blank=True)
    card_number = models.CharField(max_length=200, blank=True)
    bank_tran_id = models.CharField(max_length=200, blank=True)
    tran_date = models.DateTimeField(blank=True, editable=True, null=True)
    verify_sign = models.CharField(max_length=200, blank=True)
    risk_level = models.IntegerField(default=0)
    risk_title = models.CharField(max_length=50, blank=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Dispense Transactions"

    def __str__(self):
        return str(self.id) + " | " + str(self.gateway_type) + " | " + str(self.status)

    def gateway(self):
        return self.get_gateway_type_display()

    def dispensable_items(self):
        return CartItem.objects.filter(dispensed=False, invoice=self.invoice)

    def all_items(self):
        return CartItem.objects.filter(invoice=self.invoice)


class RFIDRechargeTransaction(models.Model):
    id = models.CharField(max_length=200, blank=False, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='recharge_user', editable=False)
    rfid_tag= models.CharField(max_length=250, blank=True)
    gateway_type = models.IntegerField(_("Gateway Type"), choices=GATEWAY_TYPES, default=1)
    product_brand = models.CharField(max_length=250, blank=True)
    product_size = models.CharField(max_length=250, blank=True)
    product_class = models.CharField(max_length=250, blank=True)
    unit_price = models.DecimalField(max_digits=9, decimal_places=2, default=0.0)
    quantity = models.PositiveIntegerField(_("Quantity"), default=0)
    status = models.CharField(_("Payment Status"), max_length=200, blank=False, default="PENDING")
    session_key = models.CharField(max_length=200, blank=True)
    validation_id = models.CharField(max_length=200, blank=True)
    amount = models.CharField(max_length=200, blank=True)
    store_amount = models.CharField(max_length=200, blank=True)
    currency = models.CharField(max_length=50, blank=True)
    card_type = models.CharField(max_length=200, blank=True)
    card_issuer = models.CharField(max_length=200, blank=True)
    card_brand = models.CharField(max_length=200, blank=True)
    card_number = models.CharField(max_length=200, blank=True)
    bank_tran_id = models.CharField(max_length=200, blank=True)
    tran_date = models.DateTimeField(blank=True, editable=True, null=True)
    verify_sign = models.CharField(max_length=200, blank=True)
    risk_level = models.IntegerField(default=0)
    risk_title = models.CharField(max_length=50, blank=True)
    ref_transaction = models.CharField(max_length=250, blank=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "RFID Recharge Transactions"

    def __str__(self):
        return str(self.id) + " | " + str(self.status)
