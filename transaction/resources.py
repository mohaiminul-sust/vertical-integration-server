from import_export import resources
from .models import Transaction, RFIDRechargeTransaction, Invoice


class InvoiceResource(resources.ModelResource):
    class Meta:
        model = Invoice
        import_id_fields = ('id',)
        fields = ('id', 'user__email', 'user__personal_info__contact_number', 'user__personal_info__first_name', 'user__personal_info__last_name', 'ru_tag', 'ru_area', 'ru_zone', 'ru_address', 'bank_transaction_fee_percentage', 'created_date', 'updated_date')


class TransactionResource(resources.ModelResource):
    class Meta:
        model = Transaction
        import_id_fields = ('id',)
        fields = ('id', 'user__email', 'invoice__id', 'rfid_tag', 'ru_tag', 'mag_no', 'status', 'dispensed', 'amount', 'store_amount', 'currency', 'card_type', 'card_issuer', 'card_brand', 'card_number', 'bank_tran_id', 'tran_date', 'created_date', 'updated_date')


class RFIDRechargeTransactionResource(resources.ModelResource):
    class Meta:
        model = RFIDRechargeTransaction
        import_id_fields = ('id',)
        fields = ('id', 'user__email', 'user__id','rfid_tag', 'status', 'quantity', 'product_brand', 'product_class', 'product_size', 'amount', 'store_amount', 'currency', 'card_type', 'card_issuer', 'card_brand', 'card_number', 'bank_tran_id', 'tran_date', 'created_date', 'updated_date')