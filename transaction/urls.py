from django.urls import path, include
from django.conf.urls import url

from .apiviews import RFIDPaymentView, TransactionListView, RFIDRechargePaymentView, RechargeSuccessView, RechargeFailedView, RechargeCanceledView, RechargeNotificationView, RFIDRechargeTransactionListView, SSLPaymentView, SSLNotificationView, SSLSuccessView, SSLFailedView, SSLCanceledView, UserDispensableListView, DispenseTransactionAPIView, UserDispensableRetrieveView, NagadAppCreateTransactionView, NagadAppTransactionPaymentView, BkashWebPaymentView, BkashPGWNotificationView

urlpatterns = [
    path("transactions/", TransactionListView.as_view(), name='transaction_list_view'),
    path("transactions/dispense/web/", DispenseTransactionAPIView.as_view(), name='transaction_dispense_view'),
    path("transactions/ssl/dispenses/", UserDispensableListView.as_view(), name='usr_dispenses_view'),
    path("transactions/ssl/dispenses/invoice/", UserDispensableRetrieveView.as_view(), name='usr_invoice_view'),
    path("transactions/payment/ssl/", SSLPaymentView.as_view(), name='ssl_payment_view'),
    path("transactions/payment/ssl/listen/", SSLNotificationView.as_view(), name='ssl_ipn'),
    path("transactions/payment/ssl/success/", SSLSuccessView.as_view(), name='ssl_success_view'),
    path("transactions/payment/ssl/failed/", SSLFailedView.as_view(), name='ssl_failed_view'),
    path("transactions/payment/ssl/cancel/", SSLCanceledView.as_view(), name='ssl_canceled_view'),

    # RFID Payment
    path("transactions/dispense/", RFIDPaymentView.as_view(), name='rfid_payment_process_view'),

    # RFID recharge endpoints
    path("recharge/transactions/", RFIDRechargeTransactionListView.as_view(), name='recharge_transaction_list_view'),
    path("recharge/payment/", RFIDRechargePaymentView.as_view(), name='recharge_payment_view'),
    path("recharge/ipn/listen/", RechargeNotificationView.as_view(), name='recharge_ipn'),
    path("recharge/status/success/", RechargeSuccessView.as_view(), name='recharge_success_view'),
    path("recharge/status/failed/", RechargeFailedView.as_view(), name='recharge_failed_view'),
    path("recharge/status/cancel/", RechargeCanceledView.as_view(), name='recharge_canceled_view'),

    # NAGAD APIs
    path("transactions/nagad/create/", NagadAppCreateTransactionView.as_view(), name='nagad_transaction_create_view'),
    path("transactions/nagad/payment/", NagadAppTransactionPaymentView.as_view(), name='nagad_transaction_payment_view'),

    # bKash PGW APIs
    path("transactions/bkash/web/create/", BkashWebPaymentView.as_view(), name='bkash_transaction_web_create_view'),
    path("transactions/bkash/web/execute/", BkashPGWNotificationView.as_view(), name='bkash_transaction_web_execute_hook'),
]