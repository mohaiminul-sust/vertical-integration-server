from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.timezone import now
from django.http import JsonResponse
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.conf import settings
from django.db.models import Sum
from django.shortcuts import redirect

import requests
import json
import datetime
import re

from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.permissions import AllowAny

from users.models import User, RFIDInfo, BilllingInfo, PersonalInfo, RFIDItem
from users.serializers import UserSerializer
from users.convenience import get_current_domain
from users.otp_handlers import update_contact_for, user_exists

from remoteunits.models import RUDevice, Magazine, MagazineProduct, RUAreaDiscount
from remoteunits.convenience import ru_response, get_area_from_tag, get_device_from_tag

from store.models import ProductType, InventoryType
from store.serializers import ProductTypeSerializer

from .models import Gateway, Transaction, RFIDRechargeTransaction, Invoice, CartItem
from .serializers import TransactionSerializer, RFIDRechargeTransactionSerializer, DispensableTransactionSerializer
from .convenience import get_new_tran_id, get_amount_with_tran_fee, get_new_invoice_id, get_group_discount, get_area_discount, notify_user_payment_status, notify_user_clicked_dispense_status, get_new_recharge_tran_id, notify_sms_generic, error_response_with_status, has_group_discount, reached_transaction_limits, reached_purchased_products_limits, check_due_amount

from .helper_sslcom import update_recharge_transaction_response_for, update_ssl_transaction_response_for
from .helper_bkash import get_bkash_grant_token, create_bkash_payment, execute_bkash_payment


class TransactionListView(generics.ListAPIView):
    """Transaction List API"""
    authentication_classes = (TokenAuthentication,)
    serializer_class = TransactionSerializer
    def get_queryset(self):
        return Transaction.objects.filter(user=self.request.user, status="PAID", tran_date__month=now().month).order_by('-tran_date')


class UserDispensableListView(APIView):
    authentication_classes = (TokenAuthentication,)
    def get(self, request):
        dispensable_transactions = Transaction.objects.filter(user=self.request.user, status="PAID", tran_date__month=now().month).order_by('-tran_date')
        distran_data = DispensableTransactionSerializer(dispensable_transactions, many=True).data
        return Response({
            "dispenses": distran_data
        })


class UserDispensableRetrieveView(APIView):
    authentication_classes = (TokenAuthentication,)
    def get(self, request):
        if 'tran_id' not in request.query_params:
            raise NotAcceptable('Must have tran_id in param!')
        tran_id = request.query_params['tran_id']
        transactions = Transaction.objects.filter(id=tran_id, user=request.user)
        if not transactions.exists():
            raise NotFound("Transaction entry not found for user")

        dispense = transactions.first()
        dispense_data = DispensableTransactionSerializer(dispense, many=False).data
        return Response({
            "dispense": dispense_data
        })


class SSLPaymentView(APIView):
    authentication_classes = (TokenAuthentication,)
    def post(self, request):
        if 'cart_items' not in request.data:
            raise NotAcceptable('Must have cart_items in request')

        if 'ru_device' not in request.data:
            raise NotAcceptable('Must have ru_device in request')

        items = request.data['cart_items']
        device = request.data['ru_device']
        gateway = Gateway.objects.filter(active=True).first()
        user = request.user
        user_has_group_discount = has_group_discount(user, get_device_from_tag(device['tag']))

        # check for user limits
        daily_limit, weekly_limit, monthly_limit = reached_transaction_limits(user)
        if daily_limit:
            raise PermissionDenied('You\'ve reached your daily dispense limit!')

        if weekly_limit:
            raise PermissionDenied('You\'ve reached your weekly dispense limit!')

        if monthly_limit:
            raise PermissionDenied('You\'ve reached your monthly dispense limit!')

        try:
            inventory_type = InventoryType.objects.get(name=items[0]['inventory_type'])
            qty = 0
            for item in items:
                qty += int(item['quantity'])
            daily_limit, weekly_limit, monthly_limit = reached_purchased_products_limits(user, inventory_type, qty)
            if daily_limit:
                raise PermissionDenied('You\'ve reached your daily product purchase limit!')

            if weekly_limit:
                raise PermissionDenied('You\'ve reached your weekly product purchase limit!')

            if monthly_limit:
                raise PermissionDenied('You\'ve reached your monthly product purchase limit!')
        except:
            pass

        try:
            invoice_id = get_new_invoice_id()
            invoice = Invoice(id=invoice_id, user=user, discount_applied=user_has_group_discount, discount_company= '' if not user_has_group_discount else user.personal_info.discount_group.name, discount_rate=0 if not user_has_group_discount else user.personal_info.discount_group.discount_rate, ru_name=device['name'], ru_address=device['address'], ru_area=device['area_name'], ru_zone = device['zone_name'], ru_tag = device['tag'], ru_lat = device['lat'], ru_lon = device['lon'], bank_transaction_fee_percentage = gateway.bank_transaction_fee_percentage)
            invoice.save()

            # Area Based discount records
            if not user_has_group_discount:
                device_area = get_area_from_tag(device['tag'])
                area_discounts = RUAreaDiscount.objects.filter(area=device_area)
                if area_discounts.exists():
                    area_discount = area_discounts.first()
                    invoice.area_discount_applied = True
                    invoice.area_discount_rate = area_discount.discount_rate
                    invoice.save()
        except:
            raise NotAcceptable('Device data-format error')

        try:
            for item in items:
                cart_item = CartItem(user=user, invoice=invoice, inventory_type=item['inventory_type'], product_brand=item['name'], product_class=item['product_class'], product_size=item['size'], unit_price=item['unit_price'], quantity=int(item['quantity']), mag_no=item['mag_no'])
                cart_item.save()
        except:
            raise NotAcceptable('Items data-format error')

        current_site = get_current_domain()
        transaction_id = get_new_tran_id()
        base_amount = round(invoice.base_total(), 2)
        d_amount = get_group_discount(base_amount, user) if user_has_group_discount else get_area_discount(base_amount, get_area_from_tag(device['tag']))
        base_amount -= round(d_amount, 2)
        total_amount = round(get_amount_with_tran_fee(base_amount, gateway.bank_transaction_fee_percentage), 2)

        if gateway.demo:
            process_url = gateway.demo_session_url
        else:
            process_url = gateway.live_session_url

        if process_url:
            payload = {
                'store_id': gateway.store_id,
                'store_passwd': gateway.store_password,
                'product_amount': base_amount,
                'total_amount': total_amount,
                'discount_amount': round(d_amount, 2),
                'currency': user.billing_info.currency,
                'tran_id': transaction_id,
                'success_url': current_site + reverse('ssl_success_view'),
                'fail_url': current_site + reverse('ssl_failed_view'),
                'cancel_url': current_site + reverse('ssl_canceled_view'),
                'cus_name': user.personal_info.first_name + " " + user.personal_info.last_name,
                'cus_email': user.email,
                'cus_add1': user.billing_info.address,
                'cus_city': user.billing_info.city,
                'cus_postcode': user.billing_info.postcode,
                'cus_country': user.billing_info.country,
                'cus_phone': user.billing_info.contact if not user.billing_info.contact == '' else user.personal_info.contact_number,
                'shipping_method': 'NO',
                'product_name': 'Purchase from {} vending machine'.format(device['name']),
                'product_category': items[0]['inventory_type'],
                'product_profile': 'physical-goods'
            }

            session_req = requests.post(process_url, data=payload)
            session_data = session_req.json()

            if session_data['status'] == "SUCCESS":
                transaction = Transaction(id=transaction_id, invoice=invoice, user=user, gateway_type=2, dispensed=False, ru_tag=device['tag'], amount=round(total_amount, 2), tran_date=now(), session_key=session_data['sessionkey'])
                transaction.save()
                return JsonResponse(
                    {
                        "session_payload": payload,
                        "ssl_gateway_data": {
                            "key": session_data['sessionkey'],
                            "redirect_uri": session_data['GatewayPageURL'], # here redirectGatewayURL can be used instead of GatewayPageURL
                            "redirect_uri_gateways": session_data['desc']
                        }
                    }
                )
            else:
                return JsonResponse(
                    {
                        "details": "Session request for payment failed!",
                        "failed_reason": session_data['failedreason']
                    }, status=status.HTTP_400_BAD_REQUEST
                )
        else:
            raise NotFound('Process URL not found for gateway')


class DispenseTransactionAPIView(APIView):
    authentication_classes = (TokenAuthentication,)
    def post(self, request):
        if 'tran_id' not in request.data:
            raise NotAcceptable('Must have tran_id in request')
        tran_id = request.data['tran_id'].strip()
        transaction = Transaction.objects.filter(id=tran_id)
        if not transaction.exists():
            raise NotFound("No transaction records found")

        transaction = transaction.first()

        # handle error for non active or disabled
        ru_device = get_device_from_tag(transaction.ru_tag)
        if ru_device is not None:
            if ru_device.disabled or not ru_device.active():
                notify_sms_generic(transaction.user, 'Sorry, this device is disabled for now! Please try out active devices or try again later.')
                raise NotAcceptable("Sorry, this device is disabled for now! Please try again later.")

        if transaction.invoice.dispensable():
            transaction.dispensed = True
            transaction.save()
            notify_user_clicked_dispense_status(transaction)
            return Response({
                'details': "Dispensed!"
            })
        else:
            raise NotAcceptable('Not dispensed!')


class NagadAppCreateTransactionView(APIView):
    authentication_classes = []
    permission_classes = []
    def post(self, request):
        # check param presence
        if 'secret' not in request.data:
            # raise NotAcceptable("Must have api secret in request")
            return error_response_with_status(msg='Must have api secret in request', status=406)
        if 'mag_id' not in request.data:
            # raise NotAcceptable("Must have mag_id in request")
            return error_response_with_status(msg='Must have mag_id in request', status=406)
        if 'contact_number' not in request.data:
            # raise NotAcceptable("Must have contact number in request")
            return error_response_with_status(msg='Must have contact number in request', status=406)

        # validate params
        api_secret = request.data['secret'].strip()
        if not api_secret == settings.API_SECRET_FOR_NAGAD:
            # raise PermissionDenied("API Secret does not match!")
            return error_response_with_status(msg='API Secret does not match!', status=403)

        mag_id = request.data['mag_id']
        try:
            device_mag = Magazine.objects.get(id=mag_id)
        except:
            # raise NotFound("Magazine not found for the given ID")
            return error_response_with_status(msg='Magazine not found for the given ID', status=404)

        device = device_mag.device
        if device.disabled:
            # raise PermissionDenied("Sorry, this device is disabled for now! Please try out active devices or try again later.")
            return error_response_with_status(msg='Sorry, this device is disabled for now! Please try out active devices or try again later.', status=403)

        qty = 1

        # get product count for magazine
        product_count = device_mag.available_count()
        if product_count < qty:
            # raise NotAcceptable("Quantity exceeds total number of available products in magazine. Available products {}".format(product_count))
            return error_response_with_status(msg='Quantity exceeds total number of available products in magazine. Available products {}'.format(product_count), status=406)

        contact_number = request.data['contact_number'].strip()

        if not re.match(settings.PHONE_VALIDATION_REGEX, contact_number):
            # raise NotAcceptable("Contact number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
            return error_response_with_status(msg='Contact number must be entered in the format: +8801711567890 . Up to 14 digits are allowed.', status=406)

        # check if user is found or create user
        user = {}
        if user_exists(contact_number):
            personal_info = PersonalInfo.objects.filter(contact_number=contact_number).first()
            user = personal_info.user
        else:
            user_mail = contact_number+"@viserveruser.com"
            user = User(username=user_mail, email=user_mail)
            user.save()
            user.set_password(contact_number)
            user.personal_info.is_otp_verified = True
            user.last_login = now()
            user.save()
            update_contact_for(user, contact_number)

        # check for user limits
        daily_limit, weekly_limit, monthly_limit = reached_transaction_limits(user)
        if daily_limit:
            return error_response_with_status(msg='You\'ve reached your daily dispense limit!', status=403)

        if weekly_limit:
            return error_response_with_status(msg='You\'ve reached your weekly dispense limit!', status=403)

        if monthly_limit:
            return error_response_with_status(msg='You\'ve reached your monthly dispense limit!', status=403)


        product_type = device_mag.product_type
        daily_limit, weekly_limit, monthly_limit = reached_purchased_products_limits(user, product_type.inventory_type)
        if daily_limit:
            return error_response_with_status(msg='You\'ve reached your daily product purchase limit!', status=403)

        if weekly_limit:
            return error_response_with_status(msg='You\'ve reached your weekly product purchase limit!', status=403)

        if monthly_limit:
            return error_response_with_status(msg='You\'ve reached your monthly product purchase limit!', status=403)


        user_has_group_discount = has_group_discount(user, device)

        # Init Trans Objs
        invoice_id = get_new_invoice_id()
        invoice = Invoice(id=invoice_id, user=user, discount_applied=user_has_group_discount, discount_company= '' if not user_has_group_discount else user.personal_info.discount_group.name, discount_rate=0 if not user_has_group_discount else user.personal_info.discount_group.discount_rate, ru_name=device.name, ru_address=device.address, ru_area=device.area.name, ru_zone = device.zone.name, ru_tag = device.tag, ru_lat = device.lat, ru_lon = device.lon, bank_transaction_fee_percentage = 0)

        if not user_has_group_discount:
            device_area = get_area_from_tag(device.tag)
            if device_area.ru_discount is not None:
                invoice.area_discount_applied = True
                invoice.area_discount_rate = device_area.ru_discount.discount_rate

        invoice.save()

        cart_item = CartItem(user=user, invoice=invoice, inventory_type=product_type.inventory_type, product_brand=product_type.name.brand, product_class=product_type.product_class.name, product_size=product_type.size.name, unit_price=product_type.unit_price, quantity=qty, mag_no=device_mag.magazine_number)
        cart_item.save()

        transaction_id = get_new_tran_id()
        base_amount = round(invoice.base_total(), 2)
        d_amount = get_group_discount(base_amount, user) if user_has_group_discount else get_area_discount(base_amount, device.area)
        total_amount = base_amount - round(d_amount, 2)
        transaction = Transaction(id=transaction_id, invoice=invoice, user=user, gateway_type=4, dispensed=False, ru_tag=device.tag, amount=total_amount, store_amount=total_amount, tran_date=now(), card_number=contact_number, card_type="App/Ussd-Nagad")
        transaction.save()

        return JsonResponse({
            "status_code": 201,
            "detail": "Transaction hook created successfully",
            "tran_id": transaction_id,
            "total_price": total_amount
        }, status=201)


class NagadAppTransactionPaymentView(APIView):
    authentication_classes = []
    permission_classes = []
    def post(self, request):
        # check param presence
        if 'secret' not in request.data:
            # raise NotAcceptable('Must have api secret in request')
            return error_response_with_status(msg='Must have api secret in request', status=406)

        if 'tran_id' not in request.data:
            # raise NotAcceptable('Must have transaction id in request')
            return error_response_with_status(msg='Must have transaction id in request', status=406)

        if 'tran_stat' not in request.data:
            # raise NotAcceptable('Must have transaction status in request')
            return error_response_with_status(msg='Must have transaction status in request', status=406)


        # validate params
        api_secret = request.data['secret'].strip()
        if not api_secret == settings.API_SECRET_FOR_NAGAD:
            # raise PermissionDenied("API Secret does not match!")
            return error_response_with_status(msg='API Secret does not match!', status=403)


        tran_stat = request.data['tran_stat'].strip()
        if tran_stat not in ('PAID', 'CANCELLED'):
            # raise NotAcceptable('Transaction status must be either PAID or CANCELLED')
            return error_response_with_status(msg='Transaction status must be either PAID or CANCELLED', status=406)


        tran_id = request.data['tran_id'].strip()
        try:
            transaction = Transaction.objects.get(id=tran_id)
        except:
            return error_response_with_status(msg='Dispensable transaction record not found for given id', status=403)

        transaction.status = tran_stat
        transaction.dispensed = True if tran_stat == 'PAID' else False
        transaction.save()

        notify_user_payment_status(transaction)

        return Response({
            "status_code": 200,
            "detail": 'Transaction State Updated'
        })


class BkashWebPaymentView(APIView):
    authentication_classes = (TokenAuthentication,)
    def post(self, request):
        if 'cart_items' not in request.data:
            raise NotAcceptable('Must have cart_items in request')

        if 'ru_device' not in request.data:
            raise NotAcceptable('Must have ru_device in request')
        items = request.data['cart_items']
        device = request.data['ru_device']

        user = request.user
        user_has_group_discount = has_group_discount(user, get_device_from_tag(device['tag']))

        # check device
        # try:
        #     ru_device = get_device_from_tag(device['tag'])
        #     if ru_device is not None:
        #         if ru_device.disabled or not ru_device.active():
        #             notify_sms_generic(user, 'Sorry, this device is disabled for now! Please try out active devices or try again later.')
        #             raise NotAcceptable("Sorry, this device is disabled for now! Please try out active devices or try again later.")
        # except:
        #     # raise NotAcceptable("Sorry, this device is not found.")
        #     raise NotAcceptable("Sorry, this device is disabled for now! Please try out active devices or try again later.")

        # check for user limits
        daily_limit, weekly_limit, monthly_limit = reached_transaction_limits(user)
        if daily_limit:
            raise PermissionDenied('You\'ve reached your daily dispense limit!')

        if weekly_limit:
            raise PermissionDenied('You\'ve reached your weekly dispense limit!')

        if monthly_limit:
            raise PermissionDenied('You\'ve reached your monthly dispense limit!')

        try:
            inventory_type = InventoryType.objects.get(name=items[0]['inventory_type'])
            qty = 0
            for item in items:
                qty += int(item['quantity'])
            daily_limit, weekly_limit, monthly_limit = reached_purchased_products_limits(user, inventory_type, qty)
            if daily_limit:
                raise PermissionDenied('You\'ve reached your daily product purchase limit!')

            if weekly_limit:
                raise PermissionDenied('You\'ve reached your weekly product purchase limit!')

            if monthly_limit:
                raise PermissionDenied('You\'ve reached your monthly product purchase limit!')
        except:
            pass

        try:
            invoice_id = get_new_invoice_id()
            invoice = Invoice(id=invoice_id, user=user, discount_applied=user_has_group_discount, discount_company= '' if not user_has_group_discount else user.personal_info.discount_group.name, discount_rate=0 if not user_has_group_discount else user.personal_info.discount_group.discount_rate, ru_name=device['name'], ru_address=device['address'], ru_area=device['area_name'], ru_zone = device['zone_name'], ru_tag = device['tag'], ru_lat = device['lat'], ru_lon = device['lon'], bank_transaction_fee_percentage = 0.0)
            invoice.save()

            # Area Based discount records
            if not user_has_group_discount:
                device_area = get_area_from_tag(device['tag'])
                area_discounts = RUAreaDiscount.objects.filter(area=device_area)
                if area_discounts.exists():
                    area_discount = area_discounts.first()
                    invoice.area_discount_applied = True
                    invoice.area_discount_rate = area_discount.discount_rate
                    invoice.save()
        except:
            raise NotAcceptable('Device data-format error')

        try:
            for item in items:
                cart_item = CartItem(user=user, invoice=invoice, inventory_type=item['inventory_type'], product_brand=item['name'], product_class=item['product_class'], product_size=item['size'], unit_price=item['unit_price'], quantity=int(item['quantity']), mag_no=item['mag_no'])
                cart_item.save()
        except:
            raise NotAcceptable('Items data-format error')

        transaction_id = get_new_tran_id()
        base_amount = round(invoice.base_total(), 2)
        d_amount = get_group_discount(base_amount, user) if user_has_group_discount else get_area_discount(base_amount, get_area_from_tag(device['tag']))
        base_amount -= round(d_amount, 2)

        token = get_bkash_grant_token()
        if token is None:
            raise NotFound("bKash token not found!")

        callback_url = get_current_domain() + reverse('bkash_transaction_web_execute_hook')
        session_data, payload = create_bkash_payment(token, str(user.id), transaction_id, base_amount, callback_url)

        if 'paymentID' not in session_data:
            return JsonResponse({
                "payload": payload,
                "grant": token,
                "data": session_data
            })

        transaction = Transaction(id=transaction_id, invoice=invoice, user=user, gateway_type=5, dispensed=False, ru_tag=device['tag'], amount=base_amount, store_amount=base_amount, tran_date=now(), session_key=session_data['paymentID'], card_type="Web/bKash")
        transaction.save()
        return JsonResponse(
            {
                "session_payload": payload,
                # "bkash_gateway_data": session_data
                "bkash_gateway_data": {
                    "payment_id": session_data['paymentID'],
                    "redirect_uri": session_data['bkashURL'],
                    "callbackUrl": session_data['callbackURL']
                }
            }
        )


class RFIDRechargeTransactionListView(generics.ListAPIView):
    """Recharge Transaction List API"""
    authentication_classes = (TokenAuthentication,)
    serializer_class = RFIDRechargeTransactionSerializer
    def get_queryset(self):
        return RFIDRechargeTransaction.objects.filter(user=self.request.user, status="PAID").order_by('-tran_date')


class RFIDPaymentView(APIView):
    authentication_classes = []
    permission_classes = []
    def post(self, request):
        # data consistency check
        if 'rfid' not in request.data:
            # raise NotAcceptable('Must include rfid in request body')
            return ru_response(0, status=406)
        if 'ru' not in request.data:
            # raise NotAcceptable('Must include ru_tag in request body')
            return ru_response(0, status=406)

        if 'mag' not in request.data:
            # raise NotAcceptable('Must include mag_no in request body')
            return ru_response(0, status=406)

        rfid_tag = request.data['rfid'].strip()
        if not RFIDInfo.objects.filter(tag=rfid_tag).exists():
            # raise NotFound('RFID not registered!')
            return ru_response(0, status=404)

        rfid = RFIDInfo.objects.filter(tag=rfid_tag).first()
        user = rfid.user

        ru_tag = request.data['ru'].strip()
        if not RUDevice.objects.filter(tag=ru_tag).exists():
            # raise NotFound('Device not registered!')
            return ru_response(0, status=404)

        device = RUDevice.objects.filter(tag=ru_tag).first()
        if device.disabled or not device.active():
            # raise PermissionDenied("Sorry, this device is disabled for now! Please try out active devices or try again later.")
            notify_sms_generic(user, 'Sorry, this device is disabled for now! Please try out active devices or try again later.')
            return ru_response(0, status=403)

        mag_no = int(request.data['mag'].strip())
        if not Magazine.objects.filter(device=device, magazine_number=mag_no).exists():
            # raise NotFound('Megazine not registered for this device!')
            return ru_response(0, status=404)

        magazine = Magazine.objects.filter(device=device, magazine_number=mag_no).first()

        # check product availability
        available_products = magazine.available_count()
        if not available_products > 0:
            # raise NotFound("No product in magazine to dispense")
            notify_sms_generic(user, 'No product in magazine to dispense')
            return ru_response(0, status=404)

        # check rfid validity, activation & balance
        if not rfid.activated:
            # raise NotAcceptable('RFID not activated!')
            notify_sms_generic(user, 'Your RFID is not activated yet!')
            return ru_response(0, status=406)

        if not rfid.is_valid():
            # raise NotAcceptable('RFID is expired!')
            notify_sms_generic(user, 'Your RFID is expired! Please renew RFID and try again.')
            return ru_response(0, status=406)

        rfid_items = RFIDItem.objects.filter(product_type=magazine.product_type, rfid=rfid, expire_date__gte=now()).order_by('expire_date')
        if not rfid_items.exists():
            # raise NotAcceptable('No Purchased Items in your RFID!')
            notify_sms_generic(user, 'You have not purchased items of this type yet for your RFID! Please refill RFID and try again.')
            return ru_response(0, status=406)

        quantity = rfid_items.aggregate(icount=Sum('quantity'))['icount']
        # rfid_item = rfid_items.first()
        # quantity = rfid_item.quantity
        if not quantity > 0:
            # raise NotAcceptable('No Purchased Items for {} in your RFID!'.format(rfid_item.product_type.name.brand))
            notify_sms_generic(user, 'No Available Items for <{}> in your RFID! Please recharge RFID and try again.'.format(rfid_item.product_type.name.brand))
            return ru_response(0, status=402)

        # check dispense limits for user
        daily_limit, weekly_limit, monthly_limit = reached_transaction_limits(user)
        if daily_limit:
            notify_sms_generic(user, 'You\'ve reached your daily dispense limit!')
            return ru_response(0, status=403)

        if weekly_limit:
            notify_sms_generic(user, 'You\'ve reached your weekly dispense limit!')
            return ru_response(0, status=403)

        if monthly_limit:
            notify_sms_generic(user, 'You\'ve reached your monthly dispense limit!')
            return ru_response(0, status=403)

        # check purchased products limit
        daily_limit, weekly_limit, monthly_limit = reached_purchased_products_limits(user, magazine.product_type.inventory_type, qty=1)
        if daily_limit:
            notify_sms_generic(user, 'You\'ve reached your daily product purchase limit!')
            return ru_response(0, status=403)

        if weekly_limit:
            notify_sms_generic(user, 'You\'ve reached your weekly product purchase limit!')
            return ru_response(0, status=403)

        if monthly_limit:
            notify_sms_generic(user, 'You\'ve reached your monthly product purchase limit!')
            return ru_response(0, status=403)

        # register ru access
        device.last_active = now()
        device.save()

        # create invoice
        invoice_id = get_new_invoice_id()
        invoice = Invoice(id=invoice_id, user=user, discount_applied=False, ru_name=device.name, ru_address=device.address, ru_area=device.area.name, ru_zone = device.zone.name, ru_tag = device.tag, ru_lat = device.lat, ru_lon = device.lon)
        invoice.save()

        # create cart entry
        pr_type = magazine.product_type
        cart_item = CartItem(user=user, invoice=invoice, inventory_type=pr_type.inventory_type.name, product_brand=pr_type.name.brand, product_class=pr_type.product_class.name, product_size=pr_type.size.name, unit_price=pr_type.unit_price, mag_no=mag_no)
        cart_item.save()

        # create transaction
        transaction_id = get_new_tran_id()
        transaction = Transaction(id=transaction_id, invoice=invoice, user=user, gateway_type=1, status='PAID', dispensed=True, rfid_tag=rfid.tag, ru_tag=ru_tag, tran_date=now(), amount=0.0, store_amount=0.0, card_type="RFID-VIL", card_number=rfid.tag)
        transaction.save()

        notify_user_clicked_dispense_status(transaction)

        return ru_response(mag_no, 1, status=200, tran_id=cart_item.id, disabled=1 if device.disabled else 0, quantity=1)


class RFIDRechargePaymentView(APIView):
    """Start recharge transaction processing - from POST params"""
    authentication_classes = (TokenAuthentication,)
    def post(self, request):
        user = request.user
        if not user.rfid_info:
            raise NotFound('No RFID registered for current user')
        if 'type_id' not in request.data:
            raise NotAcceptable('Must include type_id in request body')
        if 'qty' not in request.data:
            raise NotAcceptable('Must include qty in request body')

        # validate params
        qty = request.data['qty'].strip()
        if not int(qty) > 0:
            raise NotAcceptable("qty must be greater than 0")

        product_type = ProductType.objects.filter(id=request.data['type_id'].strip())
        if not product_type.exists():
            raise NotFound('Product type not found for given ID')
        product_type = product_type.first()

        transaction_id = get_new_recharge_tran_id()
        base_amount = round(product_type.unit_price * int(qty), 2)
        d_amount = get_group_discount(base_amount, user)
        total_amount = base_amount - round(d_amount, 2)
        total_amount = check_due_amount(user, total_amount)
        current_site = get_current_domain()
        gateway = Gateway.objects.filter(active=True).first()
        recharge_amount = round(get_amount_with_tran_fee(total_amount, gateway.bank_transaction_fee_percentage), 2)

        if gateway.demo:
            process_url = gateway.demo_session_url
        else:
            process_url = gateway.live_session_url

        if process_url:
            payload = {
                'store_id': gateway.store_id,
                'store_passwd': gateway.store_password,
                'total_amount': recharge_amount,
                'currency': user.billing_info.currency,
                'tran_id': transaction_id,
                'success_url': current_site + reverse('recharge_success_view'),
                'fail_url': current_site + reverse('recharge_failed_view'),
                'cancel_url': current_site + reverse('recharge_canceled_view'),
                'cus_name': user.personal_info.first_name + " " + user.personal_info.last_name,
                'cus_email': user.email,
                'cus_add1': user.billing_info.address,
                'cus_city': user.billing_info.city,
                'cus_postcode': user.billing_info.postcode,
                'cus_country': user.billing_info.country,
                'cus_phone': user.billing_info.contact if not user.billing_info.contact == '' else user.personal_info.contact_number,
                'shipping_method': 'NO',
                'product_name': 'Refill RFID with {} products of {} type'.format(qty, product_type.name.brand),
                'product_category': 'Electronic',
                'product_profile': 'non-physical-goods'
            }

            session_req = requests.post(process_url, data=payload)
            session_data = session_req.json()

            if session_data['status'] == "SUCCESS":
                transaction = RFIDRechargeTransaction(id=transaction_id, user=user, product_brand=product_type.name.brand, product_class=product_type.product_class.name, product_size=product_type.size.name, unit_price=product_type.unit_price, quantity=qty, rfid_tag=user.rfid_info.tag, session_key=session_data['sessionkey'], amount=round(recharge_amount, 2), tran_date=now(), gateway_type=2)
                transaction.save()
                return JsonResponse(
                    {
                        "session_payload": payload,
                        "ssl_gateway_data": {
                            "key": session_data['sessionkey'],
                            "redirect_uri": session_data['GatewayPageURL'],
                            "redirect_uri_gateways": session_data['desc']
                        }
                    }
                )
            else:
                return JsonResponse(
                    {
                        "details": "Session request for payment failed!",
                        "failed_reason": session_data['failedreason']
                    }, status=status.HTTP_400_BAD_REQUEST
                )
        else:
            raise NotFound('Process URL not found for gateway')


@permission_classes((AllowAny, ))
class SSLSuccessView(APIView):
    """Payment success redirect url for sslcommerz"""
    def post(self, request):
        return update_ssl_transaction_response_for(request)


@permission_classes((AllowAny, ))
class SSLFailedView(APIView):
    """Recharge failed redirect url for sslcommerz"""
    def post(self, request):
        return update_ssl_transaction_response_for(request)


@permission_classes((AllowAny, ))
class SSLCanceledView(APIView):
    """Recharge canceled redirect url for sslcommerz"""
    def post(self, request):
        return update_ssl_transaction_response_for(request)


@permission_classes((AllowAny, ))
class SSLNotificationView(APIView):
    """Recharge IPN notification redirect url for sslcommerz"""
    def post(self, request):
        return update_ssl_transaction_response_for(request)


@permission_classes((AllowAny, ))
class RechargeSuccessView(APIView):
    """Recharge success redirect url for sslcommerz"""
    def post(self, request):
        return update_recharge_transaction_response_for(request)


@permission_classes((AllowAny, ))
class RechargeFailedView(APIView):
    """Recharge failed redirect url for sslcommerz"""
    def post(self, request):
        return update_recharge_transaction_response_for(request)


@permission_classes((AllowAny, ))
class RechargeCanceledView(APIView):
    """Recharge canceled redirect url for sslcommerz"""
    def post(self, request):
        return update_recharge_transaction_response_for(request)


@permission_classes((AllowAny, ))
class RechargeNotificationView(APIView):
    """Recharge IPN notification redirect url for sslcommerz"""
    def post(self, request):
        return update_recharge_transaction_response_for(request)


@permission_classes((AllowAny, ))
class BkashPGWNotificationView(APIView):
    """Bkash payment execute callback hook (has required query params)"""
    def get(self, request):
        redirect_link = settings.SHOP_LINK + '/users/cart/dispenses'

        if 'paymentID' not in request.query_params:
            raise NotAcceptable("Request is missing paymentID")

        if 'status' not in request.query_params:
            raise NotAcceptable("Request is missing status")

        payment_id = request.query_params['paymentID']
        status = request.query_params['status']
        transactions = Transaction.objects.filter(gateway_type=5, session_key=payment_id)
        if not transactions.exists():
            raise NotFound("No transaction found for given paymentID")

        # update transaction state according to response
        transaction = transactions.first()
        if status == 'failure':
            transaction.status = "FAILED"
        elif status == 'cancel':
            transaction.status = "CANCELLED"
        elif status == 'success':
            transaction.status = "INITIATED"
        transaction.tran_date = now()
        transaction.save()

        # request execute payment through bkash
        if status == 'success':
            token = get_bkash_grant_token()
            if token is not None:
                status = 'success' if execute_bkash_payment(token, transaction) else 'failure'
                notify_user_payment_status(transaction)

        return redirect(redirect_link + '?status=' + status)
