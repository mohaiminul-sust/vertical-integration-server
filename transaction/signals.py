from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save

from .models import Gateway


# model signals (hooks)
@receiver(post_save, sender=Gateway)
def maintain_active_instance_singularity(sender, instance=None, created=False, **kwargs):
    other_instances = sender.objects.exclude(pk=instance.id)
    if instance.active:
        for obj in other_instances:
            obj.active = False
            obj.save()
    else:
        if not other_instances.exists():
            instance.active = True
            instance.save()