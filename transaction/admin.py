from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import Gateway, Transaction, RFIDRechargeTransaction, Invoice, CartItem
from .resources import TransactionResource, RFIDRechargeTransactionResource, InvoiceResource


admin.site.register(Gateway)


class CartItemAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'invoice', 'inventory_type', 'product_brand', 'product_class', 'product_size', 'unit_price', 'quantity', 'dispensed')
    list_filter = ('dispensed', 'inventory_type', 'product_brand', 'product_class', 'product_size',)
    search_fields = ('invoice__id', 'user__email', 'user__personal_info__contact_number', 'user__personal_info__first_name', 'user__personal_info__last_name')
    ordering = ('-created_date',)

    def has_add_permission(self, request, obj=None):
        return True

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return True

admin.site.register(CartItem, CartItemAdmin)


class TransactionInline(admin.TabularInline):
    model = Transaction
    can_delete = False
    show_change_link = True
    extra = 0
    fields = ('id', 'gateway_type', 'status', 'amount', 'tran_date', 'dispensed')

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class CartItemInline(admin.TabularInline):
    model = CartItem
    extra = 0
    show_change_link = True
    readonly_fields = ('id', 'inventory_type', 'product_brand', 'product_class', 'product_size', 'unit_price', 'quantity', 'dispensed', 'dispense_date', 'mag_no')

    def has_add_permission(self, request, obj=None):
        return False


class InvoiceAdmin(ImportExportModelAdmin):
    resource_class = InvoiceResource
    date_hierarchy = 'created_date'
    inlines = (CartItemInline, TransactionInline,)
    list_display = ('id', 'user', 'ru_name', 'dispensable', 'expiry_date', 'dispensed', 'total_products', 'total_dispensed', 'created_date')
    list_filter = ('discount_applied', 'ru_name', 'ru_area', 'ru_zone')
    search_fields = ('id', 'user__email', 'user__personal_info__contact_number', 'user__personal_info__first_name', 'user__personal_info__last_name',)
    ordering = ('-created_date',)

admin.site.register(Invoice, InvoiceAdmin)


class TransactionAdmin(ImportExportModelAdmin):
    resource_class = TransactionResource
    date_hierarchy = 'created_date'
    list_display = ('id', 'user', 'invoice','gateway_type', 'status', 'dispensed', 'amount', 'tran_date', 'created_date')
    list_filter = ('status', 'gateway_type', 'invoice__discount_company')
    search_fields = ('id', 'user__email', 'user__personal_info__contact_number', 'user__personal_info__first_name', 'user__personal_info__last_name', 'invoice__id')
    ordering = ('-tran_date',)

admin.site.register(Transaction, TransactionAdmin)


class RFIDRechargeTransactionAdmin(ImportExportModelAdmin):
    resource_class = RFIDRechargeTransactionResource
    date_hierarchy = 'created_date'
    list_display = ('id', 'user', 'gateway_type', 'status', 'amount', 'rfid_tag','card_type', 'tran_date', 'created_date')
    list_filter = ('status', 'product_brand', 'product_size', 'product_class', 'card_type')
    search_fields = ('user__email', 'user__personal_info__contact_number', 'user__personal_info__first_name', 'user__personal_info__last_name')
    ordering = ('-tran_date',)

admin.site.register(RFIDRechargeTransaction, RFIDRechargeTransactionAdmin)