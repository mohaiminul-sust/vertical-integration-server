from rest_framework.response import Response
from .models import Gateway, Transaction, RFIDRechargeTransaction
from .serializers import TransactionSerializer, RFIDRechargeTransactionSerializer
from .convenience import notify_user_payment_status, add_product_to_rfid, notify_user_rfid_recharge_status, change_rfid_recharge_transaction_status
from django.shortcuts import redirect
from django.utils.timezone import now
from django.conf import settings
import json, requests
from dateutil import parser
from decimal import Decimal


def update_ssl_transaction_response_for(request):
    redirect_front = redirect(settings.SHOP_LINK + '/users/cart/dispenses')
    tran_id = request.POST.get('tran_id', '')
    transaction = Transaction.objects.get(id=tran_id)
    tran_stat = request.POST.get('status', '')

    if transaction:
        transaction.status = tran_stat
        if tran_stat in ('VALID', 'VALIDATED'):
            transaction.validation_id = request.POST.get('val_id', '')
            transaction.amount = request.POST.get('amount', '')
            transaction.store_amount = request.POST.get('store_amount', '')
            transaction.currency = request.POST.get('currency', '')
            transaction.card_issuer = request.POST.get('card_issuer', '')
            transaction.card_brand = request.POST.get('card_brand', '')
            transaction.card_type = request.POST.get('card_type', '')
            transaction.card_number = request.POST.get('card_no', '')
            transaction.bank_tran_id = request.POST.get('bank_tran_id', '')
            transaction.tran_date = now() #parser.parse(request.POST.get('tran_date', ''))
            transaction.verify_sign = request.POST.get('verify_sign', '')
            transaction.risk_level = request.POST.get('risk_level', '')
            transaction.risk_title = request.POST.get('risk_title', '')
            transaction.status = 'PAID'
        transaction.save()

        notify_user_payment_status(transaction)

        return redirect_front
    else:
        return redirect_front


def update_recharge_transaction_response_for(request):
    redirect_front = redirect(settings.SHOP_LINK + '/users/dashboard')
    tran_id = request.POST.get('tran_id', '')
    transaction = RFIDRechargeTransaction.objects.get(id=tran_id)
    tran_stat = request.POST.get('status', '')
    if transaction:
        transaction.status = tran_stat
        if tran_stat in ('VALID', 'VALIDATED'):
            transaction.validation_id = request.POST.get('val_id', '')
            transaction.amount = request.POST.get('amount', '')
            transaction.store_amount = request.POST.get('store_amount', '')
            transaction.currency = request.POST.get('currency', '')
            transaction.card_issuer = request.POST.get('card_issuer', '')
            transaction.card_brand = request.POST.get('card_brand', '')
            transaction.card_type = request.POST.get('card_type', '')
            transaction.card_number = request.POST.get('card_no', '')
            transaction.bank_tran_id = request.POST.get('bank_tran_id', '')
            transaction.tran_date = now()
            transaction.verify_sign = request.POST.get('verify_sign', '')
            transaction.risk_level = request.POST.get('risk_level', '')
            transaction.risk_title = request.POST.get('risk_title', '')
            transaction.status = 'PAID'
            # update user rfid with products

        transaction.save()

        if transaction.status == 'PAID':
            rfid_status, rfid_item = add_product_to_rfid(transaction)
            notify_user_rfid_recharge_status(transaction, rfid_status)
            change_rfid_recharge_transaction_status(transaction.id, user=transaction.user)
        else:
            notify_user_rfid_recharge_status(transaction, "")

        return redirect_front
    else:
        return redirect_front
