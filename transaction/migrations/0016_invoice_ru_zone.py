# Generated by Django 3.0.5 on 2020-05-31 21:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transaction', '0015_auto_20200601_0044'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='ru_zone',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
