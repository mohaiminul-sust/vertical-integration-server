from django.utils.crypto import get_random_string
from .models import Transaction, Invoice, RFIDRechargeTransaction
from decimal import Decimal
from datetime import date, datetime
from django.core.mail import send_mail
from django.template import Context, loader
from django.conf import settings
from django.http import JsonResponse
from django.db.models import Sum
from django.utils.timezone import now
from dateutil.relativedelta import relativedelta
from users.otp_handlers import send_sms
from users.models import RFIDItem, UserProductLimit, UserCheckoutLimit
from store.models import ProductType, InventoryType


def get_new_tran_id():
    new_id = get_random_string(length=8)
    if Transaction.objects.filter(id=new_id).exists():
        return get_new_tran_id()
    return new_id


def get_new_recharge_tran_id():
    new_id = get_random_string(length=8)
    if RFIDRechargeTransaction.objects.filter(id=new_id).exists():
        return get_new_recharge_tran_id()
    return new_id


def get_new_invoice_id():
    new_id = get_random_string(length=8)
    if Invoice.objects.filter(id=new_id).exists():
        return get_new_invoice_id()
    return new_id


def get_amount_with_tran_fee(amount, bank_transaction_fee_percentage):
    return amount + (amount * Decimal(bank_transaction_fee_percentage/100))


def has_group_discount(user, device):
    if user.personal_info.discount_group is None:
        return False

    if device.discount_group is None:
        return False

    if device.discount_group == user.personal_info.discount_group:
        return True
    else:
        return False


def get_group_discount(base_total, user):
    if user.personal_info.discount_group is None:
        return Decimal(0)
    return base_total * Decimal(user.personal_info.discount_group.discount_rate/100)


def get_area_discount(base_total, area):
    from remoteunits.models import RUAreaDiscount

    area_discounts = RUAreaDiscount.objects.filter(area=area)

    if not area_discounts.exists():
        return Decimal(0)

    area_discount = area_discounts.first()
    return base_total * Decimal(area_discount.discount_rate/100)


def check_due_amount(user, total_amount):
    try:
        transaction = RFIDRechargeTransaction.objects.get(user=user, status='UNPAID')
        total_amount += Decimal(transaction.amount)
    except:
        total_amount = total_amount
    return total_amount


def add_product_to_rfid(transaction):
    rfid = transaction.user.rfid_info
    pr_brand = transaction.product_brand
    pr_class = transaction.product_class
    pr_size = transaction.product_size
    qty = transaction.quantity
    pr_type = ProductType.objects.filter(name__brand=pr_brand, product_class__name=pr_class, size__name=pr_size).first()
    existing_items = RFIDItem.objects.filter(rfid=rfid, product_type=pr_type, credit_type="NORMAL")
    if existing_items.exists():
        item = existing_items.first()
        item.quantity += qty
        item.save()
        return ("updated", item)
    else:
        item = RFIDItem.objects.create(rfid=rfid, product_type=pr_type, quantity=qty, expire_date=datetime(2099, 7, 25, 16))
        return ("created", item)


def notify_user_payment_status(transaction):
    user = transaction.user
    if user.can_receive_mail():
        my_template = loader.get_template('account/email/payment_status_message.txt')
        my_context = Context({
            'invoice_id': transaction.invoice.id,
            'tran_stat': transaction.status,
            'tran_id': transaction.id,
            'gateway_name': transaction.get_gateway_type_display()
        })
        payment_status_message = my_template.render(my_context.flatten())
        send_mail("Checkout Complete | Vertical Innovations", payment_status_message, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=True)
    # START - send sms
    if user.can_receive_sms():
        ack_str = 'Your payment with invoice id <{}> is {} for {}.'.format(transaction.invoice.id, transaction.status, transaction.get_gateway_type_display())
        if transaction.status == 'PAID':
            ack_str += ' Visit https://shop.vertical-innovations.com/users/cart/invoice/{} to view the invoice.'.format(transaction.id)
        else:
            ack_str += ' Please try again at your convenience.'

        send_sms(user.sms_number(), ack_str)
    # END - send sms


def change_rfid_item_status(transaction):
    rfid = transaction.user.rfid_info
    pr_brand = transaction.product_brand
    pr_class = transaction.product_class
    pr_size = transaction.product_size
    pr_type = ProductType.objects.filter(name__brand=pr_brand, product_class__name=pr_class, size__name=pr_size).first()

    unpaid_items = RFIDItem.objects.filter(rfid=rfid, product_type=pr_type, credit_type="UNPAID", expire_date__gte=now())
    paid_items = RFIDItem.objects.filter(rfid=rfid, product_type=pr_type, credit_type="NORMAL")

    if unpaid_items.exists():
        unpaid_item = unpaid_items.first()
        if paid_items.exists():
            paid_item = paid_items.first()
            paid_item.quantity += unpaid_item.quantity
            paid_item.save()
            unpaid_item.delete()
        else:
            unpaid_item.credit_type = "NORMAL"
            unpaid_item.expire_date = datetime(2099, 7, 25, 16)
            unpaid_item.save()


def change_rfid_recharge_transaction_status(id, user):
    unpaid_transactions = RFIDRechargeTransaction.objects.filter(user=user, status='UNPAID')
    if unpaid_transactions.exists():
        unpaid_transaction = unpaid_transactions.first()
        unpaid_transaction.status = 'PAID-CREDIT'
        unpaid_transaction.ref_transaction= id
        unpaid_transaction.tran_date = now()
        unpaid_transaction.save()
        change_rfid_item_status(unpaid_transaction)


def notify_user_rfid_recharge_status(transaction, rfid_status):
    user = transaction.user
    if user.can_receive_mail():
        my_template = loader.get_template('account/email/rfid_recharge_status_message.txt')
        my_context = Context({
            'tran_id': transaction.id,
            'tran_stat': transaction.status,
            'rfid_stat': rfid_status,
            'gateway_name': transaction.get_gateway_type_display(),
            'qty': transaction.quantity,
            'py_type': transaction.product_brand,
        })
        payment_status_message = my_template.render(my_context.flatten())
        send_mail("Recharge Checkout Complete | Vertical Innovations", payment_status_message, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=True)

    if user.can_receive_sms():
        ack_str = 'Your payment for RFID recharge with transaction id <{}> is {} for {}.'.format(transaction.id, transaction.status, transaction.get_gateway_type_display())
        if transaction.status == 'PAID':
            ack_str += 'Your RFID has been refilled with {} of {} products. Visit dashboard and head to RFID section to check your newly {} products.'.format(transaction.quantity, transaction.product_brand, rfid_status)
        send_sms(user.sms_number(), ack_str)


def notify_user_clicked_dispense_status(transaction):
    user = transaction.user
    if user.can_receive_mail():
        my_template = loader.get_template('account/email/user_clicked_dispense_message.txt')
        my_context = Context({
            'invoice_id': transaction.invoice.id,
            'ru_name': transaction.invoice.ru_name,
            'ru_address': transaction.invoice.ru_address
        })
        user_clicked_dispense_message = my_template.render(my_context.flatten())
        send_mail("Processing Dispense | Vertical Innovations", user_clicked_dispense_message, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=True)

    if user.can_receive_sms():
        ack_str = 'Processing dispense request for invoice <{}>. Machine will dispense products shortly once processed.'.format(transaction.invoice.id)
        send_sms(user.sms_number(), ack_str)


def notify_sms_generic(user, message):
    if user.can_receive_sms():
        send_sms(user.sms_number(), message)


def error_response_with_status(msg, status):
    return JsonResponse(
        {
            "status_code": status,
            "detail": msg
        },
        status=status
    )


def total_paid_products_by_inventory(inv_type, transactions):
    total_count = 0
    for transaction in transactions:
        items = transaction.all_items().filter(inventory_type=inv_type)
        if items.exists():
            total_count += items.aggregate(total_quantity=Sum('quantity'))['total_quantity']
    return total_count


def total_daily_transactions(user):
    transactions = Transaction.objects.filter(user=user, status="PAID", tran_date__day=now().day)
    return transactions.count()


def total_weekly_transactions(user):
    transactions = Transaction.objects.filter(user=user, status="PAID", tran_date__week=now().isocalendar()[1])
    return transactions.count()


def total_monthly_transactions(user):
    transactions = Transaction.objects.filter(user=user, status="PAID", tran_date__month=now().month)
    return transactions.count()


def total_daily_purchased_products(user, inv_type):
    transactions = Transaction.objects.filter(user=user, status="PAID", tran_date__day=now().day)
    return total_paid_products_by_inventory(inv_type, transactions)


def total_weekly_purchased_products(user, inv_type):
    transactions = Transaction.objects.filter(user=user, status="PAID", tran_date__week=now().isocalendar()[1])
    return total_paid_products_by_inventory(inv_type, transactions)


def total_monthly_purchased_products(user, inv_type):
    transactions = Transaction.objects.filter(user=user, status="PAID", tran_date__month=now().month)
    return total_paid_products_by_inventory(inv_type, transactions)


def reached_transaction_limits(user):
    reached_daily_limit = False
    reached_weekly_limit = False
    reached_monthly_limit = False
    limit_objs = UserCheckoutLimit.objects.filter(user=user)
    if limit_objs.exists():
        limit_obj = limit_objs.first()
        if limit_obj.per_day <= total_daily_transactions(user):
            reached_daily_limit = True
        if limit_obj.per_week <= total_weekly_transactions(user):
            reached_weekly_limit = True
        if limit_obj.per_month <= total_monthly_transactions(user):
            reached_monthly_limit = True
    return (reached_daily_limit, reached_weekly_limit, reached_monthly_limit)


def reached_purchased_products_limits(user, inv_type, qty=0):
    reached_daily_limit = False
    reached_weekly_limit = False
    reached_monthly_limit = False
    limit_objs = UserProductLimit.objects.filter(user=user, inventory_type=inv_type)
    if limit_objs.exists():
        limit_obj = limit_objs.first()
        if limit_obj.per_day <= (total_daily_purchased_products(user, inv_type) + qty):
            reached_daily_limit = True
        if limit_obj.per_week <= (total_weekly_purchased_products(user, inv_type) + qty):
            reached_weekly_limit = True
        if limit_obj.per_month <= (total_monthly_purchased_products(user, inv_type) + qty):
            reached_monthly_limit = True

    return (reached_daily_limit, reached_weekly_limit, reached_monthly_limit)
