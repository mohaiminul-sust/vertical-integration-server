from django.utils.translation import gettext as _


GATEWAY_TYPES = (
    (1, _("RFID")),
    (2, _("SSLCommerz")),
    (3, _("Bkash App/USSD")),
    (4, _("Nagad App/USSD")),
    (5, _("Bkash PGW Web")),
)