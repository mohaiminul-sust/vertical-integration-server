from django.conf import settings
from django.shortcuts import redirect

import json, requests, sys


def get_bkash_grant_token():
    headers = {
        'Content-Type': "application/json",
        'Accept': "application/json",
        'username': str(settings.BKASH_USERNAME),
        'password': str(settings.BKASH_PASSWORD),
    }

    payload = {
        "app_key": settings.BKASH_APP_KEY,
        "app_secret": settings.BKASH_APP_SECRET,
    }

    try:
        session_req = requests.post(settings.BKASH_API_BASE + '/tokenized/checkout/token/grant', headers=headers, data=json.dumps(payload), timeout=settings.BKASH_REQ_TIMEOUT_SECONDS)
        session_data = session_req.json()
    except requests.exceptions.Timeout:
        # return get_bkash_grant_token()
        return None
    except requests.exceptions.RequestException as e:
        return None

    if 'id_token' not in session_data:
        return None
    return session_data['id_token']


def create_bkash_payment(token, user_id, transaction_id, amount, callback_url):
    headers = {
        'Content-Type': "application/json",
        'Accept': "application/json",
        'Authorization': str(token),
        'X-App-Key': str(settings.BKASH_APP_KEY)
    }

    payload = {
        'mode': "0011",
        'payerReference': str(user_id),
        'callbackURL': callback_url,
        'amount': str(amount),
        'currency': "BDT",
        'intent': "sale",
        'merchantInvoiceNumber': transaction_id
    }

    session_req = requests.post(settings.BKASH_API_BASE + '/tokenized/checkout/create', headers=headers, data=json.dumps(payload), timeout=settings.BKASH_REQ_TIMEOUT_SECONDS)
    session_data = session_req.json()
    return (session_data, payload)


def execute_bkash_payment(token, transaction):
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': str(token),
        'X-App-Key': str(settings.BKASH_APP_KEY)
    }

    payload = {
        'paymentID': str(transaction.session_key)
    }

    try:
        session_req = requests.post(settings.BKASH_API_BASE + '/tokenized/checkout/execute', headers=headers, data=json.dumps(payload), timeout=settings.BKASH_REQ_TIMEOUT_SECONDS)
        session_data = session_req.json()
    except requests.exceptions.Timeout:
        return query_bkash_payment_status(token, transaction)
    except requests.exceptions.RequestException as e:
        # print(repr(e))
        return False

    if 'statusMessage' in session_data and session_data['statusMessage'] == "Successful":
        transaction.validation_id = session_data['trxID']
        transaction.card_number = session_data['customerMsisdn']
        transaction.status = "PAID" if session_data['transactionStatus'] == "Completed" else "NOT EXECUTED"
        transaction.save()
        return True
    # elif 'errorCode' in session_data and session_data['errorCode'] == "2029":
    #     return False
    return False


def query_bkash_payment_status(token, transaction):
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': str(token),
        'X-App-Key': str(settings.BKASH_APP_KEY)
    }

    payload = {
        'paymentID': str(transaction.session_key)
    }

    try:
        session_req = requests.post(settings.BKASH_API_BASE + '/tokenized/checkout/payment/status', headers=headers, data=json.dumps(payload), timeout=settings.BKASH_REQ_TIMEOUT_SECONDS)
        session_data = session_req.json()
    except requests.exceptions.Timeout:
        return False
    except requests.exceptions.RequestException as e:
        return False

    return True if 'transactionStatus' in session_data and session_data['transactionStatus'] == "Completed" else False