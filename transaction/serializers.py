from rest_framework import serializers
from .models import Transaction, RFIDRechargeTransaction, CartItem, Invoice
from users.models import RFIDItem


class InvoiceSerializer(serializers.ModelSerializer):
    base_total = serializers.SerializerMethodField()
    tax_total = serializers.SerializerMethodField()
    discount_amount = serializers.SerializerMethodField()
    dispensable = serializers.BooleanField()
    expiry_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    class Meta:
        model = Invoice
        exclude = ('created_date', 'updated_date', 'user')

    def get_base_total(self, obj):
        return round(obj.base_total(), 2)

    def get_tax_total(self, obj):
        return round(obj.tax_total(), 2)

    def get_discount_amount(self, obj):
        return round(obj.discount_amount(), 2)


class CartItemSerializer(serializers.ModelSerializer):
    brand = serializers.CharField(source="product_brand")
    pclass = serializers.CharField(source="product_class")
    size = serializers.CharField(source="product_size")
    price = serializers.FloatField(source="unit_price")
    qty = serializers.IntegerField(source="quantity")
    total = serializers.SerializerMethodField()
    class Meta:
        model = CartItem
        fields = ('brand', 'pclass', 'size', 'price', 'qty', 'total', 'dispensed', 'dispense_date')

    def get_total(self, obj):
        return round(obj.total_price(), 2)


class TransactionSerializer(serializers.ModelSerializer):
    tran_date = serializers.DateTimeField(format="%Y-%m-%d %I:%M:%S %p")
    provider = serializers.SerializerMethodField()
    class Meta:
        model = Transaction
        fields = ('id', 'gateway', 'amount', 'status', 'dispensed', 'tran_date', 'provider', 'invoice')

    def get_provider(self, obj):
        return obj.card_type.split('-')[-1]


class DispensableTransactionSerializer(serializers.ModelSerializer):
    tran_date = serializers.DateTimeField(format="%Y-%m-%d %I:%M:%S %p")
    invoice = InvoiceSerializer(many=False, read_only=True)
    items = CartItemSerializer(source="all_items", many=True)
    provider = serializers.SerializerMethodField()
    # rfid_products_left = serializers.SerializerMethodField()
    class Meta:
        model = Transaction
        fields = ('id', 'amount', 'gateway', 'provider', 'card_type', 'card_issuer', 'card_brand', 'card_number', 'status', 'dispensed', 'tran_date', 'invoice', 'items',)

    def get_provider(self, obj):
        return obj.card_type.split('-')[-1]

    # def get_rfid_products_left(self, obj):
    #     if not obj.gateway() == 'RFID':
    #         return None

    #     cart_items = obj.invoice.cart_items()
    #     if cart_items.exists():
    #         cart_item = cart_items.first()
    #         rfid_items = RFIDItem.objects.filter(rfid__tag=obj.rfid_tag, product_type__name__brand=cart_item.product_brand, product_type__product_class__name=cart_item.product_class, product_type__size__name=cart_item.product_size)
    #         if rfid_items.exists():
    #             return rfid_items.first().quantity
    #         else:
    #             return None
    #     else:
    #         return None


class RFIDRechargeTransactionSerializer(serializers.ModelSerializer):
    tran_date = serializers.DateTimeField(format="%Y-%m-%d %I:%M:%S %p")
    provider = serializers.SerializerMethodField()
    class Meta:
        model = RFIDRechargeTransaction
        fields = ('id', 'product_brand', 'unit_price', 'quantity', 'amount', 'provider','tran_date')

    def get_provider(self, obj):
        return obj.card_type.split('-')[-1]