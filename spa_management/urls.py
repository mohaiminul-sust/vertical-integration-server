from django.urls import path, include
from django.conf.urls import url

from .apiviews import ContactRetrieveAPIView, FAQListAPIView, YoutubeVideosListAPIView, HomePageAPIView, AdListAPIView, ShopFilterListAPIView, PlatformManagementStateApiView


urlpatterns = [
    path("spa/contact/", ContactRetrieveAPIView.as_view(), name='contact_retrieve_view'),
    path("spa/faq/", FAQListAPIView.as_view(), name='faq_list_view'),
    path("spa/videos/", YoutubeVideosListAPIView.as_view(), name='videos_list_view'),
    path("spa/home/", HomePageAPIView.as_view(), name='home_page_view'),
    path("spa/ads/", AdListAPIView.as_view(), name='ads_list_view'),
    path("spa/shop-filters/", ShopFilterListAPIView.as_view(), name='shop_filters_list_view'),
    path("spa/platform/state/", PlatformManagementStateApiView.as_view(), name='platform_state_view'),
]