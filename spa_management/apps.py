from django.apps import AppConfig


class SpaManagementConfig(AppConfig):
    name = 'spa_management'
    verbose_name = 'SPA Management'

    def ready(self):
        import spa_management.signals