from django.db import models
from django.utils.translation import gettext as _
from filer.fields.image import FilerImageField

from users.convenience import image_component
from store.models import ProductType
from .choices import ALIGNMENT_CHOICES


class Hotline(models.Model):
    phone_number = models.CharField(_("Phone Number"), max_length=150, blank=False)
    show_on_footer = models.BooleanField(_("Show On Footer"), default=False)
    show_on_header = models.BooleanField(_("Show On Header"), default=False)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Hotlines"

    def __str__(self):
        return self.phone_number


class Contact(models.Model):
    mail = models.EmailField(_("E-mail"), max_length=254, blank=False)
    fax = models.CharField(_("Fax"), max_length=254, blank=True, default="")
    address = models.TextField(_("Address"), blank=False)
    map_iframe_link = models.TextField(_("Address iframe link"), blank=False)
    company_desc = models.TextField(_("Company Description (Short)"), default="", help_text="shown in footer's left side")
    fb_link = models.CharField(_("FB Link"), max_length=200, blank=True)
    twitter_link = models.CharField(_("Twitter Link"), max_length=200, blank=True)
    insta_link = models.CharField(_("Instagram Link"), max_length=200, blank=True)
    yt_link = models.CharField(_("Youtube Link"), max_length=200, blank=True)
    linkedin_link = models.CharField(_("LinkedIn Link"), max_length=200, blank=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Contact"

    def __str__(self):
        return str(self.id)

class HomeBanner(models.Model):
    upper_text = models.CharField(_("Upper smaller Text"), max_length=40, blank=True)
    lower_text = models.CharField(_("Lower Bold Text"), max_length=30, blank=True)
    description = models.TextField(_("Description"), blank=True)
    image = FilerImageField(null=True, blank=True, related_name="home_banner_image", on_delete=models.CASCADE, help_text="Must be 1920x1080 pixels")
    link = models.CharField(_("Link"), max_length=250, blank=True)
    link_name = models.CharField(_("Link Name"), max_length=20, blank=True, help_text="will appear on the button for the link")
    active = models.BooleanField(_("Show On Homepage"), default=False)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Home Banners"

    def __str__(self):
        return str(self.id)

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'


class HomeSlider(models.Model):
    section_order = models.PositiveIntegerField(default=0, blank=False, null=False)
    upper_text = models.CharField(_("Upper smaller Text"), max_length=40, blank=True)
    lower_text = models.CharField(_("Lower Bold Text"), max_length=30, blank=True)
    align = models.IntegerField(_("Text Align"), choices=ALIGNMENT_CHOICES, default=1)
    image = FilerImageField(null=True, blank=True, related_name="home_slider_image", on_delete=models.CASCADE, help_text="Must be 1920x600 pixels")
    link = models.CharField(_("Link"), max_length=250, blank=True)
    link_name = models.CharField(_("Link Name"), max_length=20, blank=True, help_text="will appear on the button for the link")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        ordering=('section_order',)
        verbose_name_plural = "Home Sliders"

    def __str__(self):
        return str(self.id)

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'


class HomeService(models.Model):
    section_order = models.PositiveIntegerField(default=0, blank=False, null=False)
    header = models.CharField(_("Header Text"), max_length=40, blank=False)
    description = models.TextField(_("Description"), blank=False)
    image = FilerImageField(null=True, blank=True, related_name="home_services_image", on_delete=models.CASCADE, help_text="Must be 125x125 pixels")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        ordering=('section_order',)
        verbose_name_plural = "Home Services"

    def __str__(self):
        return str(self.id)

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'


class HomePartner(models.Model):
    section_order = models.PositiveIntegerField(default=0, blank=False, null=False)
    image = FilerImageField(null=True, blank=True, related_name="home_partners_image", on_delete=models.CASCADE, help_text="Must be 125x125 pixels")
    link = models.CharField(_("Link"), max_length=250, blank=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        ordering=('section_order',)
        verbose_name_plural = "Home Partners"

    def __str__(self):
        return str(self.id)

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'


class TopProduct(models.Model):
    name = models.CharField(_("Top Products"), max_length=50)
    product_types = models.ManyToManyField(ProductType, verbose_name=_("Top Product Types"))
    active = models.BooleanField(_("Show on homepage"), default=False)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Top Products"

    def __str__(self):
        return str(self.id)


class TopBlog(models.Model):
    section_order = models.PositiveIntegerField(default=0, blank=False, null=False)
    title = models.CharField(_("Title"), max_length=150, blank=False)
    author = models.CharField(_("Author"), max_length=100, blank=False)
    published_date = models.DateField(_("Published On"), auto_now=False, auto_now_add=False)
    link = models.CharField(_("Post Link"), max_length=250, blank=False)
    image = FilerImageField(null=True, blank=True, related_name="blog_thumb_image", on_delete=models.CASCADE, help_text="Must be 1000x591 pixels")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        ordering=('section_order',)
        verbose_name_plural = "Top Blogs"

    def __str__(self):
        return str(self.id)

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'


class FrequentlyAskedQuestion(models.Model):
    section_order = models.PositiveIntegerField(default=0, blank=False, null=False)
    question = models.CharField(_("Question"), max_length=250, blank=False)
    answer = models.TextField(_("Answer"), blank=False)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        ordering=('section_order',)
        verbose_name_plural = "FAQs"

    def __str__(self):
        return str(self.id)


class YoutubeVideos(models.Model):
    section_order = models.PositiveIntegerField(default=0, blank=False, null=False)
    title = models.CharField(_("Video Title"), max_length=250, blank=False)
    link = models.CharField(_("Youtube Link"), max_length=150, blank=False)
    show_on_home = models.BooleanField(_("Show on home page as a single video"), default=False)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        ordering=('section_order',)
        verbose_name_plural = "Youtube Videos"

    def __str__(self):
        return str(self.id)


class AdBanner(models.Model):
    title = models.CharField(_("Title"), max_length=50, blank=True)
    subtitle = models.CharField(_("Subtitle"), max_length=50, blank=True)
    text = models.CharField(_("Text"), max_length=50, blank=True)
    align = models.IntegerField(_("Text Align"), choices=ALIGNMENT_CHOICES, default=1)
    image = FilerImageField(null=True, blank=True, related_name="ad_banner_image", on_delete=models.CASCADE, help_text="Must be 1920x600 pixels for home page")
    featured_home = models.BooleanField(_("Featured at home top"), default=True)
    link = models.CharField(_("Link"), max_length=250, blank=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Ad Banners"

    def __str__(self):
        return str(self.id)

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'


class AdStrip(models.Model):
    text = models.CharField(_("Text"), max_length=50, blank=True)
    image = FilerImageField(null=True, blank=True, related_name="ad_strip_image", on_delete=models.CASCADE, help_text="Must be 1370 pixels of width. Preferable height 110 pixels")
    show_on_cart = models.BooleanField(_("Show on Cart Page"), default=False)
    show_on_dispense = models.BooleanField(_("Show on Invoice/Dispense Page"), default=False)
    link = models.CharField(_("Link"), max_length=250, blank=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Ad Strips"

    def __str__(self):
        return str(self.id)

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'