from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save

from .models import Hotline, YoutubeVideos, Contact, TopProduct, HomeBanner

# model signals (hooks)
@receiver(pre_save, sender=Hotline)
def hotline_fields_sanitizer(sender, instance=None, created=False, **kwargs):
    instance.phone_number = instance.phone_number.strip()


@receiver(post_save, sender=Hotline)
def header_footer_singularity_handler(sender, instance=None, created=False, **kwargs):
    if instance.show_on_footer == True:
        hotlines = Hotline.objects.exclude(id=instance.id)
        hotlines.update(show_on_footer=False)

    if instance.show_on_header == True:
        hotlines = Hotline.objects.exclude(id=instance.id)
        hotlines.update(show_on_header=False)


@receiver(pre_save, sender=Contact)
def contact_fields_sanitizer(sender, instance=None, created=False, **kwargs):
    instance.mail = instance.mail.strip()
    instance.fax = instance.fax.strip()
    instance.address = instance.address.strip()
    instance.map_iframe_link = instance.map_iframe_link.strip()
    instance.company_desc = instance.company_desc.strip()
    instance.fb_link = instance.fb_link.strip()
    instance.twitter_link = instance.twitter_link.strip()
    instance.insta_link = instance.insta_link.strip()
    instance.yt_link = instance.yt_link.strip()


@receiver(pre_save, sender=YoutubeVideos)
def video_fields_sanitizer(sender, instance=None, created=False, **kwargs):
    instance.title = instance.title.strip()
    instance.link = instance.link.strip()


@receiver(post_save, sender=YoutubeVideos)
def video_homepage_singularity_handler(sender, instance=None, created=False, **kwargs):
    if instance.show_on_home == True:
        videos = YoutubeVideos.objects.exclude(id=instance.id)
        videos.update(show_on_home=False)


@receiver(post_save, sender=TopProduct)
def top_product_singularity_handler(sender, instance=None, created=False, **kwargs):
    if instance.active == True:
        top_products = TopProduct.objects.exclude(id=instance.id)
        top_products.update(active=False)


@receiver(post_save, sender=HomeBanner)
def home_banner_singularity_handler(sender, instance=None, created=False, **kwargs):
    if instance.active == True:
        banners = HomeBanner.objects.exclude(id=instance.id)
        banners.update(active=False)