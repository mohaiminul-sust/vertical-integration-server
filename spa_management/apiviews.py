from django.shortcuts import get_object_or_404
from django.core import serializers
from django.db.models import Avg
from django.conf import settings

from rest_framework import generics, status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable
import random

from remoteunits.models import RUArea, RUZone
from remoteunits.serializers import RUAreaSerializer, RUZoneSerializer

from store.models import InventoryType, ProductCompany, ProductClass, ProductSize, ProductType
from store.serializers import InventoryTypeSerializer, ProductTypeSerializer, ProductCompanySerializer, ProductClassSerializer, ProductSizeSerializer

from .models import Hotline, Contact, FrequentlyAskedQuestion, YoutubeVideos, HomeSlider, HomeService, HomePartner, TopBlog, TopProduct, AdBanner, HomeBanner, AdStrip
from .serializers import ContactSerializer, HotlineSerializer, FrequentlyAskedQuestionSerializer, YoutubeVideosSerializer, HomeSliderSerializer, HomeServiceSerializer, HomePartnerSerializer, TopBlogSerializer, AdBannerSerializer, HomeBannerSerializer, AdStripSerializer

from platform_social.models import LikeRating
from platform_management.convenience import get_config
from transaction.models import Gateway


class ContactRetrieveAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        contact = Contact.objects.all().first()
        contact_data = ContactSerializer(contact, many=False).data

        hotlines = Hotline.objects.all()
        hotlines_data = HotlineSerializer(hotlines, many=True).data

        header_contact = Hotline.objects.filter(show_on_header=True).first()
        footer_contact = Hotline.objects.filter(show_on_footer=True).first()

        avg_rating = LikeRating.objects.filter(rating__gt=0).aggregate(Avg('rating'))['rating__avg']

        return Response({
            "avg_rating": avg_rating,
            "contact": contact_data,
            "hotline_contacts": hotlines_data,
            "header_contact": header_contact.phone_number,
            "footer_contact": footer_contact.phone_number
        })


class FAQListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        faqs = FrequentlyAskedQuestion.objects.all()
        faqs_data = FrequentlyAskedQuestionSerializer(faqs, many=True).data

        return Response({
            "faqs": faqs_data,
        })


class YoutubeVideosListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        videos = YoutubeVideos.objects.all()
        videos_data = YoutubeVideosSerializer(videos, many=True).data

        return Response({
            "videos": videos_data,
        })


class HomePageAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        active_banners = HomeBanner.objects.filter(active=True)
        banner = active_banners.first()
        banner_data = HomeBannerSerializer(banner, many=False).data

        sliders = HomeSlider.objects.all()
        sliders_data = HomeSliderSerializer(sliders, many=True).data

        services = HomeService.objects.all()
        services_data = HomeServiceSerializer(services, many=True).data

        partners = HomePartner.objects.all()
        partners_data = HomePartnerSerializer(partners, many=True).data

        inventory_types = InventoryType.objects.filter(show_on_home=True)
        inventory_types_data = InventoryTypeSerializer(inventory_types, many=True).data

        product_types = ProductType.objects.filter(inventory_type__show_on_home=True)
        product_types_data = ProductTypeSerializer(product_types, many=True).data

        home_videos = YoutubeVideos.objects.filter(show_on_home=True)
        home_video = home_videos.first()
        home_video_data = YoutubeVideosSerializer(home_video, many=False).data

        top_product = TopProduct.objects.filter(active=True).first()
        top_product_data = ProductTypeSerializer(top_product.product_types.all(), many=True).data

        top_blogs = TopBlog.objects.all()
        top_blogs_data = TopBlogSerializer(top_blogs, many=True).data

        home_ad_banners = AdBanner.objects.filter(featured_home=True)#.order_by('?')
        if home_ad_banners.exists():
            home_ad_banner = random.choice(home_ad_banners)#home_ad_banners.first()
            home_ad_banner_data = AdBannerSerializer(home_ad_banner, many=False).data
        else:
            home_ad_banner_data = None

        return Response({
            "home_ad_banner": home_ad_banner_data,
            "home_banner": banner_data if active_banners.exists() else None,
            "sliders": sliders_data,
            "services": services_data,
            "partners": partners_data,
            "inventory_types": inventory_types_data,
            "product_types": product_types_data,
            "home_video": home_video_data if home_videos.exists() else None,
            "top_products": top_product_data,
            "top_blogs": top_blogs_data,
        })


class AdListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        home_ad_banners = AdBanner.objects.filter(featured_home=True)#.order_by('?')
        if home_ad_banners.exists():
            home_ad_banner = random.choice(home_ad_banners)#home_ad_banners.first()
            home_ad_banner_data = AdBannerSerializer(home_ad_banner, many=False).data
        else:
            home_ad_banner_data = None

        cart_ad_strips = AdStrip.objects.filter(show_on_cart=True)
        if cart_ad_strips.exists():
            cart_ad_strip = random.choice(cart_ad_strips)
            cart_ad_strip_data = AdStripSerializer(cart_ad_strip, many=False).data
        else:
            cart_ad_strip_data = None

        dispense_ad_strips = AdStrip.objects.filter(show_on_dispense=True)
        if dispense_ad_strips.exists():
            dispense_ad_strip = random.choice(dispense_ad_strips)
            dispense_ad_strip_data = AdStripSerializer(dispense_ad_strip, many=False).data
        else:
            dispense_ad_strip_data = None

        return Response({
            "home_ad_banner": home_ad_banner_data,
            "cart_ad_strip": cart_ad_strip_data,
            "dispense_ad_strip": dispense_ad_strip_data,
        })


class ShopFilterListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        ru_zones = RUZone.objects.all()
        ru_zones_data = RUZoneSerializer(ru_zones, many=True).data

        ru_locations = RUArea.objects.all()
        ru_locations_data = RUAreaSerializer(ru_locations, many=True).data

        types = InventoryType.objects.filter(show_on_home=True)
        types_data = InventoryTypeSerializer(types, many=True).data

        companies = ProductCompany.objects.all().order_by('brand')
        companies_data = ProductCompanySerializer(companies, many=True).data

        classes = ProductClass.objects.all().order_by('name')
        classes_data = ProductClassSerializer(classes, many=True).data

        sizes = ProductSize.objects.all().order_by('name')
        sizes_data = ProductSizeSerializer(sizes, many=True).data

        return Response({
            "ru_zones": ru_zones_data,
            "ru_areas": ru_locations_data,
            "inventory_types": types_data,
            "product_companies": companies_data,
            "product_classes": classes_data,
            "product_sizes": sizes_data
        })


class PlatformManagementStateApiView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        pl_config = get_config()
        cart_timeout = pl_config.cart_timeout if pl_config is not None else settings.CART_HOLDOUT_HOURS
        totp_timeout = pl_config.totp_timeout if pl_config is not None else settings.TOTP_TIMEOUT_SECONDS
        bank_transaction_fee_percentage = Gateway.objects.filter(active=True).first().bank_transaction_fee_percentage
        terms_of_use = pl_config.terms_of_use
        privacy_policy = pl_config.privacy_policy

        return Response({
            'totp_timeout_seconds': totp_timeout,
            'cart_timeout_hours': cart_timeout,
            'transaction_fee_percentage': bank_transaction_fee_percentage,
            'terms': terms_of_use,
            'policy': privacy_policy
        })