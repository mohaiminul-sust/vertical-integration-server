# Generated by Django 3.0.5 on 2020-05-10 14:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('spa_management', '0011_contact_yt_link'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdBanner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('subtitle', models.CharField(max_length=50, verbose_name='Subtitle')),
                ('text', models.CharField(max_length=50, verbose_name='Text')),
                ('featured_home', models.BooleanField(default=False, verbose_name='Featured at home top')),
                ('middle_home', models.BooleanField(default=False, verbose_name='Featured at home middle')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('image', filer.fields.image.FilerImageField(blank=True, help_text='Must be 1920x1080 pixels', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ad_banner_image', to=settings.FILER_IMAGE_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Ad Banners',
            },
        ),
    ]
