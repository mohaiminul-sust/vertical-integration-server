# Generated by Django 3.0.5 on 2020-05-13 09:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('spa_management', '0016_auto_20200513_1353'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='frequentlyaskedquestion',
            options={'ordering': ('section_order',), 'verbose_name_plural': 'FAQs'},
        ),
        migrations.AlterModelOptions(
            name='homepartner',
            options={'ordering': ('section_order',), 'verbose_name_plural': 'Home Partners'},
        ),
        migrations.AlterModelOptions(
            name='homeservice',
            options={'ordering': ('section_order',), 'verbose_name_plural': 'Home Services'},
        ),
        migrations.AlterModelOptions(
            name='homeslider',
            options={'ordering': ('section_order',), 'verbose_name_plural': 'Home Sliders'},
        ),
        migrations.AlterModelOptions(
            name='topblog',
            options={'ordering': ('section_order',), 'verbose_name_plural': 'Top Blogs'},
        ),
        migrations.AlterModelOptions(
            name='youtubevideos',
            options={'ordering': ('section_order',), 'verbose_name_plural': 'Youtube Videos'},
        ),
        migrations.AddField(
            model_name='frequentlyaskedquestion',
            name='section_order',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='homepartner',
            name='section_order',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='homeservice',
            name='section_order',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='homeslider',
            name='section_order',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='topblog',
            name='section_order',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='youtubevideos',
            name='section_order',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='homeservice',
            name='image',
            field=filer.fields.image.FilerImageField(blank=True, help_text='Must be 125x125 pixels', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='home_services_image', to=settings.FILER_IMAGE_MODEL),
        ),
    ]
