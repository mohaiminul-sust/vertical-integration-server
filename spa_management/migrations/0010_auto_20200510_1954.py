# Generated by Django 3.0.5 on 2020-05-10 13:54

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('spa_management', '0009_auto_20200510_1951'),
    ]

    operations = [
        migrations.AddField(
            model_name='youtubevideos',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='youtubevideos',
            name='updated_date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
