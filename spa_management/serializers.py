from rest_framework import serializers
from users.convenience import filer_url_for

from .models import Hotline, Contact, FrequentlyAskedQuestion, YoutubeVideos, AdBanner, HomeSlider, HomeService, HomePartner, TopBlog, HomeBanner, AdStrip


class HotlineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotline
        fields = ('id', 'phone_number',)


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        exclude = ('id', 'created_date', 'updated_date',)


class FrequentlyAskedQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FrequentlyAskedQuestion
        exclude = ('created_date', 'updated_date',)


class YoutubeVideosSerializer(serializers.ModelSerializer):
    class Meta:
        model = YoutubeVideos
        fields = ('id', 'title', 'link',)


class AdBannerSerializer(serializers.ModelSerializer):
    align = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    class Meta:
        model = AdBanner
        exclude = ('id', 'featured_home', 'created_date', 'updated_date',)

    def get_image(self, obj):
        return filer_url_for(obj.image)

    def get_align(self, obj):
        return obj.get_align_display()


class AdStripSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    class Meta:
        model = AdStrip
        fields = ('text', 'image', 'link',)

    def get_image(self, obj):
        return filer_url_for(obj.image)


class HomeBannerSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    class Meta:
        model = HomeBanner
        exclude = ('id', 'active', 'created_date', 'updated_date',)

    def get_image(self, obj):
        return filer_url_for(obj.image)


class HomeSliderSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    align = serializers.SerializerMethodField()
    class Meta:
        model = HomeSlider
        exclude = ('id', 'created_date', 'updated_date',)

    def get_image(self, obj):
        return filer_url_for(obj.image)

    def get_align(self, obj):
        return obj.get_align_display()


class HomeServiceSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    class Meta:
        model = HomeService
        exclude = ('id', 'created_date', 'updated_date',)

    def get_image(self, obj):
        return filer_url_for(obj.image)


class HomePartnerSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    class Meta:
        model = HomePartner
        exclude = ('id', 'created_date', 'updated_date',)

    def get_image(self, obj):
        return filer_url_for(obj.image)


class TopBlogSerializer(serializers.ModelSerializer):
    published_date = serializers.DateField(format="%Y-%m-%d")
    image = serializers.SerializerMethodField()
    class Meta:
        model = TopBlog
        exclude = ('id', 'created_date', 'updated_date',)

    def get_image(self, obj):
        return filer_url_for(obj.image)