from django.conf import settings
from django.contrib import admin
from adminsortable2.admin import SortableAdminMixin

from .models import Hotline, Contact, HomeSlider, HomePartner, HomeService, TopProduct, TopBlog, FrequentlyAskedQuestion, YoutubeVideos, AdBanner, HomeBanner, AdStrip


class HotlineAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('phone_number', 'show_on_header', 'show_on_footer', 'created_date', 'updated_date')
    ordering = ('-created_date',)

admin.site.register(Hotline, HotlineAdmin)


class ContactAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('mail', 'address', 'created_date', 'updated_date')
    ordering = ('-created_date',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(Contact, ContactAdmin)


class HomeBannerAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'image_preview','upper_text', 'lower_text', 'created_date','updated_date')
    ordering = ('-created_date',)

admin.site.register(HomeBanner, HomeBannerAdmin)


class HomeSliderAdmin(SortableAdminMixin, admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'image_preview','upper_text', 'lower_text', 'created_date','updated_date')

admin.site.register(HomeSlider, HomeSliderAdmin)


class HomeServiceAdmin(SortableAdminMixin, admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'header', 'image_preview', 'description', 'created_date', 'updated_date')

    def has_add_permission(self, request, obj=None):
        if HomeService.objects.all().count() < settings.MAXIMUM_SERVICES_IN_SPA:
            return True
        else:
            return False

admin.site.register(HomeService, HomeServiceAdmin)


class HomePartnerAdmin(SortableAdminMixin, admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'image_preview', 'link','created_date', 'updated_date')

admin.site.register(HomePartner, HomePartnerAdmin)


class TopProductAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('name', 'top_product_types', 'active', 'created_date', 'updated_date')
    ordering = ('-created_date',)
    filter_horizontal = ('product_types',)

    def top_product_types(self, obj):
        return obj.product_types.count()

admin.site.register(TopProduct, TopProductAdmin)


class TopBlogAdmin(SortableAdminMixin, admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('title', 'author', 'image_preview', 'published_date', 'created_date')

admin.site.register(TopBlog, TopBlogAdmin)


class FrequentlyAskedQuestionAdmin(SortableAdminMixin, admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('question', 'created_date', 'updated_date')

admin.site.register(FrequentlyAskedQuestion, FrequentlyAskedQuestionAdmin)


class YoutubeVideosAdmin(SortableAdminMixin, admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('title', 'link', 'created_date', 'updated_date')

admin.site.register(YoutubeVideos, YoutubeVideosAdmin)


class AdBannerAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'image_preview', 'featured_home', 'link','title', 'subtitle', 'created_date', 'updated_date')
    ordering = ('-created_date',)

admin.site.register(AdBanner, AdBannerAdmin)


class AdStripAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'image_preview', 'text', 'link', 'created_date', 'updated_date')
    list_filter = ('show_on_cart', 'show_on_dispense')
    ordering = ('-created_date',)

admin.site.register(AdStrip, AdStripAdmin)