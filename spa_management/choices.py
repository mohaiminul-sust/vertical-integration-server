from django.utils.translation import gettext as _


ALIGNMENT_CHOICES = (
    (1, _("left")),
    (2, _("right")),
    (3, _("center")),
)