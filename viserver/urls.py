"""viserver URL Configuration

"""

from django.conf.urls import url
from django.conf import settings
from django.conf.urls import handler404
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, reverse_lazy
from django.views.generic.base import RedirectView

from rest_framework.documentation import include_docs_urls


admin.site.site_header = "Vertical Innovations"
admin.site.site_title = "Vertical Innovations"
admin.site.index_title = "Dashboard"
admin.site.site_url = settings.SHOP_LINK

doc_title = 'VIL BaaS API Documentation'
handler404 = 'users.views.error_404_view'

urlpatterns = [
    url(r'^$', RedirectView.as_view(url=reverse_lazy('admin:index'))),
    url(r'^admin/', admin.site.urls),
    path(r'', include('users.urls')),
    path(r'', include('remoteunits.urls')),
    path(r'', include('transaction.urls')),
    path(r'', include('platform_social.urls')),
    path(r'', include('spa_management.urls')),
    url(r'^filer/', include('filer.urls')),
    path(r'api-docs/', include_docs_urls(title=doc_title))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# def trigger_error(request):
#     division_by_zero = 1 / 0

# urlpatterns += [
#     path('sentry-debug/', trigger_error),
# ]