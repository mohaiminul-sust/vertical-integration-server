"""
Django settings for viserver project.

Generated by 'django-admin startproject' using Django 1.11.20.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="https://53f9f7b2eb094f55a1282dac733a9107@o46969.ingest.sentry.io/5270545",
    integrations=[DjangoIntegration()],

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True
)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SHOP_LINK = "https://shop.vertical-innovations.com"

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'stt595u(5i*ir8g60@90vvk*bz_2u!!b^*pb!e2v_rmj6q)***'

# Current Site for URL generation
SITE_ID = 1

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SPA Caps
MAXIMUM_SERVICES_IN_SPA = 4

# Cart Holdout Time
CART_HOLDOUT_HOURS = 12

# Magazine Threshold
RU_MAGAZINE_LENGTH = 25
RU_MAGAZINE_THRESHOLD = 5

# Product SKU Length
PRODUCT_SKU_TAIL_LENGTH = 4

RATING_MAX_LIMIT = 5

# OTP Length
# Max Value = 8
OTP_DIGITS_LENGTH = 6
TOTP_TIMEOUT_SECONDS = 180

# SMS provider credentials
# SMS_ACCOUNT_KEY = "12c4b514"
# SMS_ACCOUNT_SECRET = "GebZnjkWUHvthsl8"
# SMS_SENDER_SLUG = "Vertical"

SMS_API_BASE = "https://sms.nixtecsys.com/index.php"
SMS_API_USER = "Vertical"
SMS_API_TOKEN = "03cbec40c0ad0ce89b7c0a96cb3e5f66ee75aae0"

# Phone number validation regex
PHONE_VALIDATION_REGEX = r'^\+88\d{11}$' #r'^(\+\d{1,3})?,?\s?\d{8,13}$'

# Nagad API Secret
API_SECRET_FOR_NAGAD = 'ir8g60@90vvk*bz_2u'

# # bKash PGW Sandbox Creds
# BKASH_API_BASE = 'https://tokenized.sandbox.bka.sh/v1.2.0-beta'
# BKASH_APP_KEY = '4f6o0cjiki2rfm34kfdadl1eqq'
# BKASH_APP_SECRET = '2is7hdktrekvrbljjh44ll3d9l1dtjo4pasmjvs5vl5qr3fug4b'
# BKASH_USERNAME = 'sandboxTokenizedUser02'
# BKASH_PASSWORD = 'sandboxTokenizedUser02@12345'

# bKash PGW Production Creds
BKASH_API_BASE = 'https://tokenized.pay.bka.sh/v1.2.0-beta'
BKASH_APP_KEY = '7lutca64osjbp1o9bbofea1adh'
BKASH_APP_SECRET = '1r0vml0bgrvmmlroj34sugcj1bhlnloua2relohra51me2f0mr9b'
BKASH_USERNAME = 'VERTICALINNOVATIONSTC'
BKASH_PASSWORD = 'vE6@tiC9Lir1hK'
BKASH_REQ_TIMEOUT_SECONDS = 30


ALLOWED_HOSTS = [
    'dash.vertical-innovations.com',
    '161.35.10.49',
    'localhost',
]

# Filer Params
FILE_UPLOAD_PERMISSIONS = 0o660
FILER_CANONICAL_URL = 'sharing/'
THUMBNAIL_HIGH_RESOLUTION = True
THUMBNAIL_QUALITY = 95
THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)
FILER_ENABLE_PERMISSIONS = False
FILER_PAGINATE_BY = 50
FILER_DUMP_PAYLOAD = False


# Media
MEDIA_URL =  '/uploads/'
MEDIA_ROOT = os.path.join(BASE_DIR, "uploads/")

AVATAR_IMAGE_PLACEHOLDER = "placeholders/avatar.png"

# admin password reset timeout param
PASSWORD_RESET_TIMEOUT_DAYS = 3

# Application definition
X_FRAME_OPTIONS='SAMEORIGIN'


INSTALLED_APPS = [
    'admin_interface',
    'colorfield',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.sites',
    'django.contrib.humanize',

    # rest auth
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',

    # all auth
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.linkedin_oauth2',
    'allauth.socialaccount.providers.google',
    'rest_auth.registration',

    # filer deps
    'easy_thumbnails',
    'filer',
    'mptt',

    # anymail
    'anymail',

    # dev extensions
    'django_extensions',
    'import_export',
    'adminsortable2',
    'mapwidgets',
    'ckeditor',

    # platform apps/modules
    'users',
    'store',
    'remoteunits',
    'transaction',
    'platform_management',
    'platform_social',
    'spa_management',
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'viserver.urls'

TEMPLATES_ROOT = os.path.join(BASE_DIR, "templates/")
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATES_ROOT],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'viserver.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    # sqlite configs
    # 'default': {
    #     'ENGINE': 'django.db.backends.sqlite3',
    #     'NAME': os.path.join(BASE_DIR, 'verticaldb.sqlite3'),
    # }
    # postgres configs
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',#'django.db.backends.postgresql_psycopg2',
        'NAME': 'viserver',
        'USER': 'viadmin',
        'PASSWORD': 'viadmin',
        'HOST': 'localhost',
        'PORT': '',
    }
}

MAP_WIDGETS = {
    "GooglePointFieldWidget": (
        ("zoom", 15),
        ("mapCenterLocationName", "dhaka"),
        ("GooglePlaceAutocompleteOptions", {'componentRestrictions': {'country': 'bd'}}),
        ("markerFitZoom", 12),
    ),
    "GOOGLE_MAP_API_KEY": "AIzaSyAQp5pLSt-adXzF9oPwk65fxvuVdtyxVeo"
}

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            ['RemoveFormat', 'Source']
        ]
    },
}

AUTH_USER_MODEL = 'users.User'
# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Asia/Dhaka'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static/")


# Account Settings
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend', # Needed to login by username in Django
    'allauth.account.auth_backends.AuthenticationBackend', # allauth
)
DEFAULT_FROM_EMAIL = 'noreply@vertical-innovations.com'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
ANYMAIL = {
    "MAILGUN_API_KEY": "cc4713ae6c02fb10ffaaa152b6e97c45-a2b91229-ded35f4f",
    "MAILGUN_SENDER_DOMAIN": 'sandboxe77e642ea57f4b3a83e1bfbe8ce68d0c.mailgun.org',
}
EMAIL_BACKEND =  'anymail.backends.mailgun.EmailBackend' #'django.core.mail.backends.console.EmailBackend'

# ACCOUNT_DEFAULT_HTTP_PROTOCOL = 'https'
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = 'none' #mandatory, optional
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_ADAPTER = 'users.adapter.LoginAccountAdapter'
OLD_PASSWORD_FIELD_ENABLED = False
LOGOUT_ON_PASSWORD_CHANGE = False


# Rest Framework
REST_AUTH_SERIALIZERS = {
    'PASSWORD_RESET_SERIALIZER': 'users.serializers.PasswordResetSerializer',
    # 'PASSWORD_RESET_CONFIRM_SERIALIZER': 'users.serializers.PasswordResetConfirmSerializer',
}

REST_FRAMEWORK = {
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 50,
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
}