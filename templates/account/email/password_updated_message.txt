{% load i18n %}{% blocktrans with site_name='Vertical Innovations' %}
Hi,
Your password has been successfully updated for {{ site_name }}.
{% endblocktrans %}
Follow the link below to access dashboard.
{% block redirect_link %}
https://shop.vertical-innovations.com/users/dashboard
{% endblock %}
{% blocktrans with site_name='Strawberry' %}Thank you for choosing {{ site_name }}!{% endblocktrans %}
