from django.contrib import admin

from .models import LikeRating, UserFeedback
from .forms import LikeRatingForm


class LikeRatingAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('user', 'liked', 'rating', 'created_date', 'updated_date')
    list_filter= ('liked',)
    form = LikeRatingForm

admin.site.register(LikeRating, LikeRatingAdmin)


class UserFeedbackAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('contact', 'review', 'created_date', 'updated_date')

admin.site.register(UserFeedback, UserFeedbackAdmin)