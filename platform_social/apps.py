from django.apps import AppConfig


class PlatformSocialConfig(AppConfig):
    name = 'platform_social'
    verbose_name = 'User Engagement'