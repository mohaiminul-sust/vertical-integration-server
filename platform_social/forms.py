from django.forms import ModelForm
from django.conf import settings

from decimal import Decimal
from .models import LikeRating


class LikeRatingForm(ModelForm):
    class Meta:
        model = LikeRating
        fields = '__all__'

    def clean(self):
        liked = self.cleaned_data.get('liked')
        rating = self.cleaned_data.get('rating')

        if rating > Decimal(settings.RATING_MAX_LIMIT):
            msg = "Maximum value is 5.00!"
            self._errors['liked'] = self.error_class([msg])
            del self.cleaned_data['liked']

        return self.cleaned_data