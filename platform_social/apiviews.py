from django.shortcuts import get_object_or_404
from django.core import serializers
from rest_framework import generics, status, viewsets
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable
from rest_framework.authentication import TokenAuthentication


from .models import LikeRating, UserFeedback
from .serializers import LikeRatingSerializer, UserFeedbackSerializer


class LikeRatingRetrieveUpdateAPIViewSet(generics.RetrieveUpdateAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = LikeRating.objects.all()
    serializer_class = LikeRatingSerializer
    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(user=self.request.user)
        if obj:
            self.check_object_permissions(self.request, obj)
            return obj
        else:
            raise NotFound("No user data found!")

    def perform_update(self, serializer):
        instance = serializer.save(user=self.request.user)
        return instance


class UserFeedbackListCreateAPIViewSet(generics.ListCreateAPIView):
    authentication_classes = []
    permission_classes = []
    queryset = UserFeedback.objects.all()
    serializer_class = UserFeedbackSerializer