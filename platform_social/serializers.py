from rest_framework import serializers

from .models import LikeRating, UserFeedback


class LikeRatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = LikeRating
        exclude = ('id', 'user', 'created_date', 'updated_date',)


class UserFeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserFeedback
        exclude = ('id', 'created_date', 'updated_date',)
