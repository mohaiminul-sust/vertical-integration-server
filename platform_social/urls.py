from django.urls import path, include
from django.conf.urls import url

from .apiviews import LikeRatingRetrieveUpdateAPIViewSet, UserFeedbackListCreateAPIViewSet


urlpatterns = [
    url(r'^like-rating/$', LikeRatingRetrieveUpdateAPIViewSet.as_view(), name='like_rating_retrieve_update_views'),
    url(r'^feedbacks/$', UserFeedbackListCreateAPIViewSet.as_view(), name='user_feedback_list_create_views'),
]