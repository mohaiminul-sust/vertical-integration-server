from django.db import models
from django.utils.translation import gettext as _

from users.models import User


class LikeRating(models.Model):
    user = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE, related_name='like_rating')
    liked = models.BooleanField(_("Liked"), default=False)
    rating = models.DecimalField(_("Rating"), max_digits=3, decimal_places=2, default=0)
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Likes & Ratings"

    def __str__(self):
        return self.user.email if self.user.email is not '' else str(self.user.id)


class UserFeedback(models.Model):
    contact = models.CharField(_("E-mail/Contact"), max_length=254, blank=False)
    review = models.TextField(_("Review"))
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Feedbacks / Reviews"

    def __str__(self):
        return str(self.id)