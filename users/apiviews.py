from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from django.utils.http import urlencode
from django.core import serializers
from django.utils.crypto import get_random_string
from django.core.mail import send_mail
from django.template import Context, loader
from django.conf import settings

from rest_framework import generics, status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view

from rest_auth.registration.views import LoginView, RegisterView, VerifyEmailView

from .models import User, PersonalInfo, UserOtp, BilllingInfo, RFIDInfo, ForgetPassword
from .serializers import UserSerializer, PersonalInfoSerializer, AccountRegisterSerializer, BillingInfoSerializer, RFIDInfoSerializer
from .convenience import get_parts_from_name
from .otp_handlers import otp_request_handler, otp_verification_handler, send_sms
from store.models import InventoryType


#API Views
class RestRegisterView(RegisterView):
    authentication_classes = (TokenAuthentication,)
    serializer_class = AccountRegisterSerializer


class RestLoginView(LoginView):
    authentication_classes = (TokenAuthentication,)


class RestVerifyEmailView(VerifyEmailView):
    authentication_classes = (TokenAuthentication,)


@api_view()
def null_view(request):
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view()
def complete_view(request):
    return Response("Email account is activated")


class PersonalInfoViewSet(generics.RetrieveUpdateAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = PersonalInfo.objects.all()
    serializer_class = PersonalInfoSerializer
    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(user=self.request.user)
        if obj:
            self.check_object_permissions(self.request, obj)
            return obj
        else:
            raise NotFound("No user data found!")

    def perform_update(self, serializer):
        if 'full_name' not in self.request.data:
            instance = serializer.save(user=self.request.user)
            return instance

        full_name = str(self.request.data['full_name'])
        full_name = full_name.strip()
        if not full_name == '' or full_name is not None:
            first, last = get_parts_from_name(full_name)
            user = self.request.user
            user.first_name = first
            user.last_name = last
            user.save()
            instance = serializer.save(user=user, first_name=first, last_name=last)
            return instance
        else:
            instance = serializer.save(user=self.request.user)
            return instance


class BillingInfoViewSet(generics.RetrieveUpdateAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = BilllingInfo.objects.all()
    serializer_class = BillingInfoSerializer
    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(user=self.request.user)
        if obj:
            self.check_object_permissions(self.request, obj)
            return obj
        else:
            raise NotFound("No user data found!")

    def perform_update(self, serializer):
        instance = serializer.save(user=self.request.user)
        return instance


class RFIDInfoViewSet(generics.RetrieveAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = RFIDInfo.objects.all()
    serializer_class = RFIDInfoSerializer
    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        try:
            obj = queryset.get(user=self.request.user)
            if obj:
                self.check_object_permissions(self.request, obj)
                return obj
            else:
                raise NotFound("No rfid data found!")
        except:
            raise NotFound("RFID data not found for user!")


# OTP APIViews
class OTPRequestAPIView(APIView):
    authentication_classes = (TokenAuthentication,)
    def post(self, request):
        otp_obj = otp_request_handler(request, authenticated=True)
        return Response({
            'detail': 'message sent!',
            'otp_sid': otp_obj.sid
        })


class OTPRequestGuestAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def post(self, request):
        otp_obj = otp_request_handler(request, authenticated=False)
        return Response({
            'detail': 'message sent!',
            'otp_sid': otp_obj.sid
        })


class OTPVerifyAPIView(APIView):
    authentication_classes = (TokenAuthentication,)
    def post(self, request):
        otp_status = otp_verification_handler(request, authenticated=True)
        return Response({
            'detail': 'User verified!',
            'otp_status': otp_status,
        })


class OTPVerifyGuestAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def post(self, request):
        otp_status = otp_verification_handler(request, authenticated=False)
        return Response({
            'detail': 'User verified!',
            'otp_status': otp_status
        })


class ForgetPasswordAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def post(self, request):
        if 'email' not in request.data:
            raise NotAcceptable('Must include email in request')
        email = request.data['email'].strip()

        ForgetPassword.objects.create(email=email)

        user = User.objects.filter(email=email)
        if user.exists():
            user= user.first()

            new_pass = get_random_string(length=16)
            user.set_password(new_pass)
            user.save()

            if user.can_receive_mail():
                my_template = loader.get_template('account/email/password_forgot_message.txt')
                my_context = Context({
                    'temp_pass': new_pass,
                })
                forgot_pass_message = my_template.render(my_context.flatten())
                send_mail("Forgot your password!", forgot_pass_message, settings.DEFAULT_FROM_EMAIL, [email], fail_silently=True)

            if user.can_receive_sms():
                send_sms(user.sms_number(), 'Your password has been temorarily set to {}. Go to https://shop.vertical-innovations.com/users/change-password to update the password as you like it.'.format(new_pass))

            return Response({
                'detail': 'Mail Sent!',
            })
        else:
            raise NotFound('You\'re not registered!')