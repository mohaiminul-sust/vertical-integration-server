from django.db import models
from django.db.models import Sum
from uuid import uuid4
from django.contrib.auth.models import AbstractUser
from .usermanager import UserManager

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.utils.timezone import now
from django.utils.translation import gettext as _
from django.core.validators import RegexValidator
from django.conf import settings

from .convenience import image_component
from platform_management.convenience import get_config
from store.models import ProductType, InventoryType


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    email = models.EmailField(max_length=254, unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def __str__(self):
        return self.email if self.email.strip() != '' else str(self.id)

    def assigned_group(self):
        return self.groups.values_list('name', flat = True)[0] if self.groups.exists() else '-'

    def can_receive_sms(self):
        return self.personal_info.is_otp_verified if self.personal_info is not None and not self.personal_info.contact_number == '' else False

    def sms_number(self):
        return self.personal_info.contact_number

    def can_receive_mail(self):
        mail = self.email
        mail_domain = mail.split('@')[-1]
        return False if mail_domain == 'viserveruser.com' else True


class DiscountGroup(models.Model):
    name = models.CharField(_("Company Name"), max_length=150, blank=False, null=False, default='')
    discount_rate = models.DecimalField(_("Discount Rate"), max_digits=5, decimal_places=2, help_text='in percentage (%)', default=0)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Company based Discount Groups"

    def __str__(self):
        return self.name + " | " + str(self.discount_rate) + '%'


class ForgetPassword(models.Model):
    email = models.CharField(_("E-mail"), max_length=254, default='')
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Forget Password Requests"

    def __str__(self):
        return self.email


class UserOtp(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='otp', help_text="Select User")
    sent_to = models.CharField(_("Sent To"), max_length=17, blank=True, null=True, help_text="Phone Number")
    otp = models.IntegerField(blank=True, null=True, help_text="Generated OTP")
    sid = models.CharField(max_length=150, blank=True, null=True, help_text="Sender ID for OTP")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "User's OTP Log"

    def __str__(self):
        return self.sent_to + " | " + str(self.otp)

    def is_expired(self):
        time_delta = now() - self.created_date
        platform_config = get_config()
        timeout_seconds = platform_config.totp_timeout if platform_config is not None else settings.TOTP_TIMEOUT_SECONDS
        if time_delta.seconds > timeout_seconds:
            return True
        else:
            return False


class PersonalInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name='personal_info')
    first_name = models.CharField(max_length=150, blank=True, null=True, help_text="First Name")
    last_name = models.CharField(max_length=150, blank=True, null=True, help_text="Last Name")
    date_of_birth = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True, help_text="Birthday")
    contact_regex = RegexValidator(regex=settings.PHONE_VALIDATION_REGEX, message="Contact number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    contact_number = models.CharField(validators=[contact_regex], max_length=17, null=False, blank=True, help_text="Phone Number", default="")
    address = models.TextField(null=True, blank=True, help_text="Address")
    lat = models.FloatField(_("Latitude"), blank=True, default=0.0)
    lon = models.FloatField(_("Longitude"), blank=True, default=0.0)
    is_otp_verified = models.BooleanField(_("OTP Verified"), default=False)
    discount_group = models.ForeignKey(DiscountGroup, on_delete=models.CASCADE, null=True, blank=True, related_name='discount_group', help_text="Select Discount Group")
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Personal Infos"

    def __str__(self):
        return self.user.email if self.user.email is not '' else str(self.user.id)

    def calculated_age(self):
        return relativedelta(date.today(), self.date_of_birth).years

    def full_name(self):
        return self.first_name + " " + self.last_name if not self.first_name == '' or not self.last_name == '' else ''


class BilllingInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name='billing_info')
    address = models.CharField(max_length=200, blank=True, help_text="Street Address")
    city = models.CharField(max_length=200, blank=True, help_text="City")
    postcode = models.CharField(max_length=200, blank=True, help_text="Zipcode")
    country = models.CharField(max_length=200, blank=True, help_text="Country name", default="Bangladesh")
    contact = models.CharField(max_length=200, blank=True, help_text="Contact info")
    currency = models.CharField(max_length=200, blank=False, default="BDT", help_text="Billing currency")
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Billing Infos"

    def __str__(self):
        return self.user.email if self.user.email is not '' else str(self.user.id)


class RFIDInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name='rfid_info')
    tag = models.CharField(_("RFID Tag"), max_length=250, help_text="RFID Tag Number")
    activated = models.BooleanField(_("Activated"), default=False)
    valid_until = models.DateTimeField(_("Valid Until"), auto_now=False, auto_now_add=False, default=now)
    last_used = models.DateTimeField(_("Last Used"), auto_now=False, auto_now_add=False, default=now)
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "RFID Infos"

    def __str__(self):
        user_str = self.user.email if self.user.email is not '' else str(self.user.id)
        # return self.tag if self.tag.strip() is not "" else user_str
        tag = self.tag if self.tag.strip() is not "" else "NO TAG"
        return user_str + " | " + tag

    def is_valid(self):
        return True if self.valid_until > now() else False
    is_valid.boolean = True

    def total_products(self):
        return RFIDItem.objects.filter(rfid=self).aggregate(total_qty=Sum('quantity'))['total_qty']

    def products(self):
        return RFIDItem.objects.filter(rfid=self)

    def total_types(self):
        return RFIDItem.objects.filter(rfid=self).count()


class RFIDItem(models.Model):
    class CreditType(models.TextChoices):
        FREE = "FREE"
        UNPAID = "UNPAID"
        NORMAL = "NORMAL"
        #CONDTIONAL = "condtional", "Condtional"
    rfid = models.ForeignKey(RFIDInfo, on_delete=models.CASCADE, null=True, blank=True, related_name='rfid_item_rfid', help_text="Select RFID")
    product_type = models.ForeignKey(ProductType, on_delete=models.CASCADE, null=True, blank=True, related_name='rfid_item_pr_type', help_text="Select Product Type")
    credit_type = models.CharField("Credit Type", max_length=20, choices=CreditType.choices, default=CreditType.NORMAL)
    quantity = models.PositiveIntegerField(_("Quantity"), default=0)
    expire_date = models.DateTimeField(default=datetime(2099, 7, 25, 16), help_text="Only applicable for 'UNPAID' credit type")
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "RFID Products"
        unique_together = [['rfid', 'product_type', 'credit_type']]

    def __str__(self):
        return str(self.id)


class UserProductLimit(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='product_limit_user', help_text="Select User")
    inventory_type = models.ForeignKey(InventoryType, on_delete=models.CASCADE, null=True, blank=True, related_name='product_limit_inv_type', help_text="Select Inventory Type")
    per_day = models.PositiveIntegerField(_("Daily Limit"), default=0)
    per_week = models.PositiveIntegerField(_("Weekly Limit"), default=0)
    per_month = models.PositiveIntegerField(_("Monthly Limit"), default=0)
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "User Product Limits"
        unique_together = [['user', 'inventory_type']]

    def __str__(self):
        return self.user.__str__() + ' | ' + str(self.id)


class UserCheckoutLimit(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name='checkout_limit_user', help_text="Select User")
    per_day = models.PositiveIntegerField(_("Daily Limit"), default=0)
    per_week = models.PositiveIntegerField(_("Weekly Limit"), default=0)
    per_month = models.PositiveIntegerField(_("Monthly Limit"), default=0)
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "User Checkout Limits"

    def __str__(self):
        return self.user.__str__()
