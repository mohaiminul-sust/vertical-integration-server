from django.conf import settings
from django.utils.timezone import now
from random import randint
# from nexmo import Client
import requests

from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable
from rest_framework.authtoken.models import Token

from .models import UserOtp, User, PersonalInfo
from platform_management.convenience import get_config

import re


def send_sms(number, message):
    # key = settings.SMS_ACCOUNT_KEY
    # secret = settings.SMS_ACCOUNT_SECRET
    # client = Client(key=key, secret=secret)
    # response = client.send_message({
    #     'from': settings.SMS_SENDER_SLUG,
    #     'to': str(number),
    #     'text': message
    # })
    # response = response['messages'][0]
    # if not response['status'] == '0':
    #     # raise NotFound("Message not sent!")
    #     return "Not send!"
    # return response['message-id']
    params = {
        "app": "webservices",
        "ta": "pv",
        "u": str(settings.SMS_API_USER),
        "h": str(settings.SMS_API_TOKEN),
        "to": str(number),
        "msg": message,
    }
    response = requests.get(settings.SMS_API_BASE, params=params)
    try:
        return response.text.split(',')[-1]
    except:
        return "NOT SENT"

def generate_otp():
    platform_config = get_config()
    n = platform_config.otp_digits_length if platform_config is not None else settings.OTP_DIGITS_LENGTH
    range_start = 10**(n-1)
    range_end = (10**n)-1
    otp_code = randint(range_start, range_end)
    if UserOtp.objects.filter(otp=otp_code).count() == 0:
        return otp_code
    else:
        return generate_otp()


def update_contact_for(user, contact):
    # update user contact number
    personal_info = user.personal_info
    personal_info.contact_number = contact
    personal_info.save()

    billing_info = user.billing_info
    if billing_info.contact == '':
        billing_info.contact = contact
        billing_info.save()


def user_exists(contact_number):
    return PersonalInfo.objects.filter(contact_number=contact_number).exists()


def otp_request_handler(request, authenticated=False):
    if 'contact_number' not in request.data:
        raise NotAcceptable('Must include contact_number in request body')

    contact_number = request.data['contact_number']
    if not re.match(settings.PHONE_VALIDATION_REGEX, contact_number):
        raise NotAcceptable("Contact number must be entered in the format: '+999999999'. Up to 15 digits allowed.")

    # generate
    otp = generate_otp()
    # prep message
    message = "Your code is <" + str(otp) + ">. Use this code to verify your vertical innovations account."

    # sent message & record otp info against user
    nexmo_sid = send_sms(contact_number, message)
    otp_obj = UserOtp.objects.create(user=request.user if authenticated else None, otp=otp, sid=nexmo_sid, sent_to=contact_number)
    return otp_obj


def otp_verification_handler(request, authenticated=False):
    if 'otp_code' not in request.data:
        raise NotAcceptable('Must include otp_code in request body')

    if 'sender_id' not in request.data:
        raise NotAcceptable('Must include sender_id in request body')

    otp_code = request.data['otp_code']
    sender_id = request.data['sender_id']

    # check for otp
    otp_obj = UserOtp.objects.filter(sid=sender_id, otp=otp_code).latest('created_date')
    if otp_obj is None:
        raise NotFound("OTP record not found for this user")

    # check if otp is expired
    if otp_obj.is_expired():
        raise PermissionDenied("OTP is expired. Request another OTP.")

    # otp confirmed, user creation / alteration
    if authenticated:
        # update user verified status
        user = request.user
        user.personal_info.is_otp_verified = True
        user.save()

        update_contact_for(user, otp_obj.sent_to)

        token, created = Token.objects.get_or_create(user=user)
        return {
            'user_created': False,
            'key': token.key
        }
    else:
        user_mail = otp_obj.sent_to+"@viserveruser.com"
        user = {}
        user_created = True
        if user_exists(otp_obj.sent_to):
            # raise NotAcceptable("User already registered! Use proper gateway for authenticated users")
            personal_info = PersonalInfo.objects.filter(contact_number=otp_obj.sent_to).first()
            user = personal_info.user
            user_created = False
        else:
            # user = User(username=user_mail, email=user_mail)
            # user.save()
            user = User.objects.create(username=user_mail, email=user_mail)
            user.set_password(otp_obj.sent_to)

        user.personal_info.is_otp_verified = True
        user.last_login = now()
        user.save()

        otp_obj.user = user
        otp_obj.save()
        update_contact_for(user, otp_obj.sent_to)
        if user.can_receive_sms():
            send_sms(user.sms_number(), 'Your Registration to vertical innovations shop is successful. Login & visit your dashboard to update details.')

        token, created = Token.objects.get_or_create(user=user)
        return {
            'user_created': user_created,
            'key': token.key
        }