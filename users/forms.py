from django.forms import ModelForm
from django.utils.timezone import now

from transaction.models import RFIDRechargeTransaction
from .models import RFIDItem, RFIDInfo


class RFIDItemForm(ModelForm):
    class Meta:
        model = RFIDItem
        fields = '__all__'

    def clean(self):
        rfid = self.cleaned_data.get('rfid')
        quantity = self.cleaned_data.get('quantity')
        credit_type = self.cleaned_data.get('credit_type')
        expire_date = self.cleaned_data.get('expire_date')

        if credit_type == "UNPAID":
            try:
                rfid_info = RFIDInfo.objects.get(user=rfid)
                if RFIDRechargeTransaction.objects.filter(user=rfid_info.user, status="UNPAID").exists():
                    msg = "Already has unpaid products!"
                    self._errors['credit_type'] = self.error_class([msg])
                    del self.cleaned_data['credit_type']
            except:
                pass

        if quantity == 0:
            msg = "Must enter quantity greater than zero"
            self._errors['quantity'] = self.error_class([msg])
            del self.cleaned_data['quantity']

        if credit_type == "UNPAID" and expire_date < now():
            msg = "Exipre date can't be past dates!"
            self._errors['expire_date'] = self.error_class([msg])
            del self.cleaned_data['expire_date']