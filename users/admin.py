from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
# from django.contrib.auth.models import Permission
# from django.conf import settings

from .models import User, UserOtp, PersonalInfo, RFIDInfo, BilllingInfo, ForgetPassword, DiscountGroup, RFIDItem, UserProductLimit, UserCheckoutLimit
from .convenience import RFIDIsValidFilter
from .forms import RFIDItemForm

# for debugging permission objects (e.g. deleting leftovers from unused/deleted models)
# admin.site.register(Permission)

class PersonalInfoInline(admin.StackedInline):
    model = PersonalInfo
    can_delete = False


class BillingInfoInline(admin.StackedInline):
    model = BilllingInfo
    can_delete = False


class UserProductLimitInline(admin.TabularInline):
    model = UserProductLimit
    fields = ('inventory_type', 'per_day', 'per_week', 'per_month')
    extra = 0


class UserCheckoutLimitInline(admin.StackedInline):
    model = UserCheckoutLimit


class RFIDInfoInline(admin.StackedInline):
    model = RFIDInfo
    can_delete = False
    show_change_link = True


class UserAdmin(UserAdmin):
    date_hierarchy = 'date_joined'
    inlines = (PersonalInfoInline, BillingInfoInline, RFIDInfoInline, UserProductLimitInline, UserCheckoutLimitInline)
    list_display = ('email', 'id', 'account_type', 'staff_status', 'last_login', 'date_joined')
    list_filter = ('groups', 'is_staff')
    search_fields = ('email',)

    def account_type(self, obj):
        return obj.assigned_group()

    def staff_status(self, obj):
        return obj.is_staff
    staff_status.boolean = True

    def get_inlines(self, request, obj=None):
        if obj:
            return self.inlines
        else:
            return []

admin.site.register(User, UserAdmin)


class DiscountGroupAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('name', 'discount_rate', 'created_date', 'updated_date')
    ordering = ('name',)
    search_fields = ('name',)

admin.site.register(DiscountGroup, DiscountGroupAdmin)


class ForgetPasswordAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('email', 'created_date')
    ordering = ('-created_date',)
    search_fields = ('email',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return True

admin.site.register(ForgetPassword, ForgetPasswordAdmin)


class UserOtpAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('user', 'sent_to', 'otp', 'sid','created_date', 'valid')
    ordering = ('-created_date',)
    search_fields = ('user__email', 'sent_to', 'otp')

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def valid(self, obj):
        return not obj.is_expired()
    valid.boolean = True

admin.site.register(UserOtp, UserOtpAdmin)


class PersonalInfoAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('user', 'first_name', 'last_name', 'date_of_birth', 'contact_number', 'otp_verified', 'discount_group')
    search_fields = ('user__email', 'contact_number', 'first_name', 'last_name')
    list_filter = ('is_otp_verified', 'user__groups', 'discount_group')
    ordering = ('-created_date',)

    def has_add_permission(self, request, obj=None):
        return True

    def otp_verified(self, obj):
        return obj.is_otp_verified
    otp_verified.boolean = True

admin.site.register(PersonalInfo, PersonalInfoAdmin)


class BillingInfoAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('user', 'address', 'city', 'postcode', 'country', 'contact', 'currency')
    search_fields = ('user__email', 'contact', 'user__personal_info__contact_number')
    list_filter = ('city', 'currency')
    ordering = ('-created_date',)

    def has_add_permission(self, request, obj=None):
        return True

admin.site.register(BilllingInfo, BillingInfoAdmin)


class UserCheckoutLimitAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('user', 'per_day', 'per_week', 'per_month', 'created_date', 'updated_date')
    search_fields = ('user__email', 'user__personal_info__contact_number')
    list_filter = ('per_day', 'per_week', 'per_month')
    ordering = ('-created_date',)


admin.site.register(UserCheckoutLimit, UserCheckoutLimitAdmin)


class UserProductLimitAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('user', 'inventory_type', 'per_day', 'per_week', 'per_month', 'created_date', 'updated_date')
    search_fields = ('user__email', 'inventory_type__name', 'user__personal_info__contact_number')
    list_filter = ('inventory_type', 'per_day', 'per_week', 'per_month')
    ordering = ('-created_date',)


admin.site.register(UserProductLimit, UserProductLimitAdmin)


class RFIDItemInline(admin.TabularInline):
    model = RFIDItem
    readonly_fields = ('credit_type', 'expire_date', 'product_type', 'quantity', 'created_date')
    extra = 0
    can_delete = False
    show_change_link = True

    def has_add_permission(self, request, obj=None):
        return False


class RFIDInfoAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    inlines = (RFIDItemInline,)
    list_display = ('user', 'tag', 'activated', 'is_valid')
    search_fields = ('user__email', 'tag')
    list_filter = (RFIDIsValidFilter, 'activated',)
    ordering = ('-created_date',)

admin.site.register(RFIDInfo, RFIDInfoAdmin)


class RFIDItemAdmin(admin.ModelAdmin):
    form = RFIDItemForm
    date_hierarchy = 'created_date'
    list_display = ('id', 'rfid', 'product_type', 'credit_type', 'quantity','created_date',)
    search_fields = ('rfid__user__email', 'rfid__tag', 'product_type__name__brand', 'product_type__inventory_type__name')
    list_filter = ('credit_type', 'product_type', 'product_type__inventory_type',)
    ordering = ('-created_date',)

admin.site.register(RFIDItem, RFIDItemAdmin)
