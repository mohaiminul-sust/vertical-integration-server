from allauth.account.signals import user_signed_up, password_changed, email_confirmed
# from allauth.socialaccount.signals import social_account_added
from django.contrib.auth.signals import user_logged_in
from django.core.mail import send_mail
from django.template import Context, loader
from django.conf import settings
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save
from datetime import datetime
from django.utils.timezone import now

from .models import User, PersonalInfo, BilllingInfo, RFIDItem
from transaction.models import RFIDRechargeTransaction, Gateway
from transaction.convenience import get_new_recharge_tran_id, get_group_discount, get_amount_with_tran_fee
from users.convenience import get_current_domain
from .otp_handlers import send_sms
from platform_social.models import LikeRating


# model signals (hooks)
@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        PersonalInfo.objects.create(user=instance, first_name=instance.first_name, last_name=instance.last_name)
        BilllingInfo.objects.create(user=instance)
        LikeRating.objects.create(user=instance)
    else:
        info = PersonalInfo.objects.get(user=instance)
        info.first_name = instance.first_name
        info.last_name = instance.last_name
        info.save()

# @receiver(post_save, sender=User)
# def create_related_models(sender, instance=None, created=False, **kwargs):
#     if created:
#         PersonalInfo.objects.create(user=instance, first_name=instance.first_name, last_name=instance.last_name)
#         BilllingInfo.objects.create(user=instance)
#         LikeRating.objects.create(user=instance)
#     else:
#         info = PersonalInfo.objects.get(user=instance)
#         info.first_name = instance.first_name
#         info.last_name = instance.last_name
#         info.save()


# login signal
@receiver(user_logged_in)
def user_logged_in_handler(sender, user, request, **kwargs):
    user.last_login = now()
    user.save()


# allauth signals
@receiver(user_signed_up)
def user_signed_up(request, user, **kwargs):
    if user.can_receive_mail():
        my_template = loader.get_template('account/email/after_signup_message.txt')
        my_context = Context({
            'user': user,
        })
        after_signup_email_message = my_template.render(my_context.flatten())
        send_mail("Welcome to Vertical Innovations!", after_signup_email_message,
            settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=True)


@receiver(password_changed)
def password_changed(request, user, **kwargs):
    if user.can_receive_sms():
        send_sms(user.sms_number(), 'Your password has been successfully updated.')

    if user.can_receive_mail():
        my_template = loader.get_template('account/email/password_updated_message.txt')
        my_context = Context({
            'user': user,
        })
        pass_update_email_message = my_template.render(my_context.flatten())
        send_mail("Password Updated | Vertical Innovations", pass_update_email_message,
            settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=True)


@receiver(email_confirmed)
def email_confirmed(request, email_address, **kwargs):
    my_template = loader.get_template('account/email/after_email_confirmed_message.txt')
    my_context = Context({
        'email': email_address,
    })
    after_email_verify_message = my_template.render(my_context.flatten())
    send_mail("Email Verified | Vertical Innovations", after_email_verify_message,
          settings.DEFAULT_FROM_EMAIL, [email_address], fail_silently=True)


# @receiver(social_account_added)
# def social_account_added(request, sociallogin, **kwargs):
#     user = sociallogin.account.user
#     my_template = loader.get_template('account/email/social_account_connected_message.txt')
#     my_context = Context({
#         'user': user,
#     })
#     social_account_connected_message = my_template.render(my_context.flatten())
#     send_mail("Social Account connected with VI!", social_account_connected_message,
#           settings.DEFAULT_FROM_EMAIL, [user.email])


@receiver(post_save, sender=RFIDItem)
def create_related_models(sender, instance=None, created=False, **kwargs):
    print(instance)
    if created:
        if instance.credit_type == "NORMAL":
            return
        transaction_id = get_new_recharge_tran_id()
        user = instance.rfid.user
        qty = instance.quantity
        product_type =instance.product_type
        base_amount = round(product_type.unit_price * int(qty), 2)
        if instance.credit_type == 'UNPAID':
            d_amount = get_group_discount(base_amount, user)
        else:
            d_amount = base_amount
        status = instance.credit_type
        total_amount = base_amount - round(d_amount, 2)
        print(base_amount, d_amount, total_amount)
        gateway = Gateway.objects.filter(active=True).first()
        recharge_amount = round(get_amount_with_tran_fee(total_amount, gateway.bank_transaction_fee_percentage), 2)
        transaction = RFIDRechargeTransaction(id=transaction_id, user=user, product_brand=product_type.name.brand, product_class=product_type.product_class.name, product_size=product_type.size.name, unit_price=product_type.unit_price, quantity=qty, rfid_tag=user.rfid_info.tag, status=status, amount=round(recharge_amount, 2), tran_date=now(), gateway_type=2)
        transaction.save()


