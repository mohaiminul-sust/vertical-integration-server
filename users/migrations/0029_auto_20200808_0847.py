# Generated by Django 3.0.5 on 2020-08-08 02:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0053_inventorytype_unit_id_text'),
        ('users', '0028_usercheckoutlimit'),
    ]

    operations = [
        migrations.AddField(
            model_name='rfiditem',
            name='credit_type',
            field=models.CharField(choices=[('FREE', 'Free'), ('UNPAID', 'Unpaid'), ('NORMAL', 'Normal')], default='NORMAL', max_length=20, verbose_name='Credit Type'),
        ),
        migrations.AlterUniqueTogether(
            name='rfiditem',
            unique_together={('rfid', 'product_type', 'credit_type')},
        ),
    ]
