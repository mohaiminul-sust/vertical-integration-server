from rest_framework import serializers
from users.convenience import filer_url_for

from .models import InventoryType, ProductType, ProductClass, ProductCompany, ProductSize


class InventoryTypeSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    align = serializers.SerializerMethodField()
    class Meta:
        model = InventoryType
        exclude = ('pr_sku_code', 'section_order', 'created_date', 'updated_date',)

    def get_image(self, obj):
        return filer_url_for(obj.image)

    def get_align(self, obj):
        return obj.get_align_display()


class ProductClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductClass
        fields = ('id', 'name',)


class ProductCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCompany
        fields = ('id', 'brand',)


class ProductSizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductSize
        fields = ('id', 'name',)


class ProductTypeSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source="name.brand")
    image = serializers.SerializerMethodField()
    inventory_type = serializers.CharField(source="inventory_type.name")
    inventory_type_unit_title = serializers.CharField(source="inventory_type.unit_id_text")
    product_class = serializers.CharField(source="product_class.name")
    product_class_id = serializers.IntegerField(source="product_class.id")
    product_brand_id = serializers.IntegerField(source="name.id")
    brand_idx = serializers.CharField(source="name.pr_sku_code")
    size = serializers.CharField(source="size.name")
    size_id = serializers.IntegerField(source="size.id")
    max_capacity = serializers.IntegerField(source="inventory_type.max_capacity")
    class Meta:
        model = ProductType
        exclude = ('aux_size', 'created_date', 'updated_date',)

    def get_image(self, obj):
        return filer_url_for(obj.image)