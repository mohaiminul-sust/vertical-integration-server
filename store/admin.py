# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from adminsortable2.admin import SortableAdminMixin

from .models import ProductType, Product, SingleProduct, InventoryType, Warehouse, ProductClass, ProductSize, ProductCompany
from .resources import ProductResource


class InventoryTypeAdmin(SortableAdminMixin, admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('name', 'pr_sku_code', 'image_preview', 'show_on_home', 'unit_id_text', 'max_capacity', 'created_date', 'updated_date')
    # ordering = ('name',)

admin.site.register(InventoryType, InventoryTypeAdmin)


class ProductClassAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('name', 'pr_sku_code', 'created_date', 'updated_date')
    ordering = ('name',)

admin.site.register(ProductClass, ProductClassAdmin)


class ProductCompanyAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('brand', 'pr_sku_code', 'created_date', 'updated_date')
    ordering = ('brand',)

admin.site.register(ProductCompany, ProductCompanyAdmin)


class ProductSizeAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('name', 'pr_sku_code', 'created_date', 'updated_date')
    ordering = ('name',)

admin.site.register(ProductSize, ProductSizeAdmin)


class ProductTypeAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'name', 'image_preview', 'size', 'product_class', 'unit_price', 'inventory_type', 'updated_date')
    list_filter = ('size', 'product_class', 'inventory_type')
    ordering = ('id',)

admin.site.register(ProductType, ProductTypeAdmin)


class ProductInline(admin.TabularInline):
    model = Product
    readonly_fields = ('id', 'sku', 'product_type', 'unit_price','warehouse', 'count', 'valid', 'valid_until')
    show_change_link = True
    can_delete = False

    def get_extra(self, request, obj=None, **kwargs):
        return 0

    def has_add_permission(self, request, obj=None):
        return False


class WarehouseAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    inlines = [ProductInline]
    list_display = ('name', 'pr_sku_code', 'address', 'created_date', 'updated_date')
    search_fields = ('name',)
    ordering = ('-created_date',)

admin.site.register(Warehouse, WarehouseAdmin)


class ProductAdmin(ImportExportModelAdmin):
    resource_class = ProductResource
    date_hierarchy = 'created_date'
    list_display = ('id', 'sku', 'product_type', 'unit_price','warehouse', 'count', 'valid', 'valid_until')
    list_filter = ('product_type__name', 'product_type__product_class', 'product_type__size')
    search_fields = ('sku', 'product_type__name', 'product_type__product_class',)
    ordering = ('-created_date',)

admin.site.register(Product, ProductAdmin)


class SingleProductAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'product', 'valid', 'valid_until', 'created_date')
    list_filter = ('product',)
    search_fields = ('product__sku',)
    ordering = ('-created_date',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(SingleProduct, SingleProductAdmin)