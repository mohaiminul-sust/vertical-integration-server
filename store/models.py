from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext as _
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from filer.fields.image import FilerImageField

from users.convenience import image_component
from spa_management.choices import ALIGNMENT_CHOICES


class InventoryType(models.Model):
    section_order = models.PositiveIntegerField(default=0, blank=False, null=False)
    name = models.CharField(_("Name"), max_length=50, blank=False, null=False)
    pr_sku_code = models.CharField(_("Code for Product SKU"), max_length=2, blank=False, null=False, default='')
    align = models.IntegerField(_("Text Align"), choices=ALIGNMENT_CHOICES, default=1)
    unit_id_text = models.CharField(_("Unit Title"), max_length=50, help_text="Unit identifier title text e.g kg, piece, ltr etc.", null=True, blank=False)
    max_capacity = models.PositiveIntegerField(_("Max Capacity"), default=25)
    show_on_home = models.BooleanField(_("Show On Home"), default=True)
    image = FilerImageField(null=True, blank=True, related_name="inventory_type_image", on_delete=models.CASCADE, help_text="Upload/Select image for this type of inventory")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        ordering = ('section_order',)
        verbose_name_plural = "Inventory Types"
        unique_together = [['name', 'pr_sku_code']]

    def __str__(self):
        return self.name + " | " + self.pr_sku_code

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'


class ProductClass(models.Model):
    name = models.CharField(_("Name"), max_length=50, blank=False)
    pr_sku_code = models.CharField(_("Code for Product SKU"), max_length=2, blank=False, null=False, default='', unique=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Product Classes"

    def __str__(self):
        return self.name + " | " + self.pr_sku_code


class ProductCompany(models.Model):
    brand = models.CharField(_("Brand Name"), max_length=50, blank=False, default='')
    pr_sku_code = models.CharField(_("Code for Product SKU"), max_length=2, blank=False, null=False, default='', unique=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Product Company"

    def __str__(self):
        return self.brand + " | " + self.pr_sku_code


class ProductSize(models.Model):
    name = models.CharField(_("Name"), max_length=50, blank=False)
    pr_sku_code = models.CharField(_("Code for Product SKU"), max_length=3, blank=False, null=False, default='', unique=True)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Product Sizes"

    def __str__(self):
        return self.name + " | " + self.pr_sku_code


class ProductType(models.Model):
    name = models.ForeignKey(ProductCompany, on_delete=models.CASCADE, null=False, blank=False, related_name='company', help_text="Select company name")
    image = FilerImageField(null=True, blank=True, related_name="product_type_image", on_delete=models.CASCADE, help_text="Upload/Select image for this type of product")
    inventory_type = models.ForeignKey(InventoryType, on_delete=models.CASCADE, null=False, blank=False, related_name='inventory_type', help_text="Select inventory type")
    unit_price = models.DecimalField(_("Unit Price"), max_digits=9, decimal_places=2, default=0.0)
    product_class = models.ForeignKey(ProductClass, on_delete=models.CASCADE, null=False, blank=False, related_name='type_class', help_text="Select product class")
    size = models.ForeignKey(ProductSize, on_delete=models.CASCADE, null=False, blank=False, related_name='type_class', help_text="Select product class")
    aux_size = models.CharField(_("Size (extra)"), max_length=50, blank=True, null=False, default='')
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Product Types"
        unique_together = [['name', 'product_class', 'size']]

    def __str__(self):
        return self.name.brand + ' | ' + self.product_class.name + ' | ' + self.size.name

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'


class Warehouse(models.Model):
    name = models.CharField(_("Name"), max_length=50, blank=False, null=False)
    address = models.TextField(_("Address"), blank=False)
    pr_sku_code = models.CharField(_("Code for Product SKU"), max_length=2, blank=False, null=False, default='')
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        unique_together = [['name', 'pr_sku_code']]

    def __str__(self):
        return self.name


class Product(models.Model):
    sku = models.CharField(_("SKU"), max_length=100, blank=True, unique=True,help_text="Store ID")
    product_type = models.ForeignKey(ProductType, on_delete=models.CASCADE, null=False, blank=False, related_name='product_type', help_text="Select product type")
    count= models.PositiveIntegerField(default=0)
    valid_until = models.DateTimeField(_("Validity Date"), auto_now=False, auto_now_add=False, default=now)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE, null=False, blank=False, related_name='warehouse', help_text="Select warehouse")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.sku if self.sku is not None or not self.sku.strip() == '' else str(self.id)

    def unit_price(self):
        return self.product_type.unit_price

    def valid(self):
        return True if self.valid_until > now() else False
    valid.boolean = True

    def inventory_code(self):
        return self.product_type.inventory_type.pr_sku_code

    def warehouse_code(self):
        return self.warehouse.pr_sku_code

    def company_code(self):
        return self.product_type.name.pr_sku_code

    def class_code(self):
        return self.product_type.product_class.pr_sku_code

    def size_code(self):
        return self.product_type.size.pr_sku_code

    def aux_size_code(self):
        return self.product_type.aux_size

    def valid_month_code(self):
        valid_month = self.valid_until.month
        if valid_month <= 9:
            return str(valid_month)
        else:
            if valid_month == 10:
                return 'A'
            elif valid_month == 11:
                return 'B'
            elif valid_month == 12:
                return 'C'

    def valid_year_code(self):
        valid_year = self.valid_until.year
        valid_year = list(str(valid_year))
        return valid_year[-1]


class SingleProduct(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True, blank=True, related_name='product', help_text="Select product")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return str(self.id) + ' | ' + self.product.__str__()

    def valid(self):
        return self.product.valid()
    valid.boolean = True

    def valid_until(self):
        return self.product.valid_until