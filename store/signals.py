from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save

from .models import SingleProduct, Product
from .convenience import get_new_product_sku


# model signals (hooks)
@receiver(pre_save, sender=Product)
def auto_generate_product_sku(sender, instance=None, created=False, **kwargs):
    # assign auto SKU
    if instance is not None and instance.sku.strip() == '':
        instance.sku = get_new_product_sku(instance)


@receiver(post_save, sender=Product)
def bulk_generate_single_product(sender, instance=None, created=False, **kwargs):
    # check for existing entries for product
    existing_count = SingleProduct.objects.filter(product=instance).count()

    if existing_count < instance.count:
        # bulk create required single products
        single_products = [SingleProduct(product=instance) for i in range(instance.count - existing_count)]
        SingleProduct.objects.bulk_create(single_products)
    elif existing_count > instance.count:
        delete_count = existing_count - instance.count
        sp_to_delete = SingleProduct.objects.filter(product=instance).order_by('created_date')[:delete_count]
        for sp in sp_to_delete:
            sp.delete()