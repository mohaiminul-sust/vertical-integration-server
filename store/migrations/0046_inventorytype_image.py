# Generated by Django 3.0.5 on 2020-05-10 09:45

from django.conf import settings
from django.db import migrations
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('store', '0045_auto_20200510_1541'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventorytype',
            name='image',
            field=filer.fields.image.FilerImageField(blank=True, help_text='Upload/Select image for this type of inventory', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='inventory_type_image', to=settings.FILER_IMAGE_MODEL),
        ),
    ]
