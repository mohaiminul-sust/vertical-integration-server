# Generated by Django 3.0.5 on 2020-04-23 20:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0032_auto_20200424_0249'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='producttype',
            options={'verbose_name_plural': 'Product Types'},
        ),
        migrations.AlterUniqueTogether(
            name='producttype',
            unique_together={('name', 'product_class', 'size')},
        ),
    ]
