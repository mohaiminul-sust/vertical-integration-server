# Generated by Django 3.0.5 on 2020-04-19 15:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0014_auto_20200415_2114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='sku',
            field=models.CharField(blank=True, help_text='Store ID', max_length=100, unique=True, verbose_name='SKU'),
        ),
    ]
