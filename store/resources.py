from import_export import resources
from .models import Product

class ProductResource(resources.ModelResource):

    class Meta:
        model = Product
        import_id_fields = ('sku',)
        fields = ('sku', 'product_type', 'count', 'valid_until', 'created_date', 'updated_date')