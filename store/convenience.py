from django.utils.crypto import get_random_string
from django.conf import settings

from .models import Product
from platform_management.convenience import get_config


def get_new_product_sku(product):
    # get product sku head
    head_str = ''
    head_str += product.inventory_code()
    head_str += product.warehouse_code()
    head_str += product.company_code()
    head_str += product.class_code()
    head_str += product.size_code()
    head_str += product.aux_size_code()
    head_str += product.valid_month_code()
    head_str += product.valid_year_code()

    # make random sku tail
    platform_config = get_config()
    tail_len = platform_config.product_sku_tail_length if platform_config is not None else settings.PRODUCT_SKU_TAIL_LENGTH
    tail_str = get_random_string(length=tail_len)

    # concate with '-' separator
    new_sku = head_str.upper() + '-' + tail_str.upper()

    # check duplicate
    if Product.objects.filter(sku=new_sku).exists():
        return get_new_product_sku(product)
    return new_sku