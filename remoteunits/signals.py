from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, m2m_changed
from django.contrib.gis.geos import Point

import hashlib

from .models import RUDevice
from .convenience import get_new_ru_tag

# model signals (hooks)
@receiver(pre_save, sender=RUDevice)
def generate_tag_for_ru(sender, instance=None, created=False, **kwargs):
    if instance.tag is None or instance.tag.strip() == '':
        # encoded_date = str(instance.created_date).encode()
        # hash_engine = hashlib.sha1()
        # hash_engine.update(encoded_date)
        # instance.tag = hash_engine.hexdigest()
        instance.tag = get_new_ru_tag()

    try:
        instance.loc = Point(instance.lon, instance.lat, srid=4326)
    except:
        instance.loc = None