from django.db import models
from django.contrib.gis.db import models
from django.db.models import Sum

# Create your models here.
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.utils.timezone import now
from django.utils.translation import gettext as _
from django.conf import settings
from filer.fields.image import FilerImageField

from users.convenience import image_component
from users.models import DiscountGroup
from store.models import ProductType
from platform_management.convenience import get_config
from transaction.models import CartItem


class RUZone(models.Model):
    name = models.CharField(_("Zone Name"), max_length=100, blank=False, unique=True, help_text="Device Name")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Device Zones"

    def __str__(self):
        return self.name


class RUArea(models.Model):
    name = models.CharField(_("Area Name"), max_length=100, blank=False, unique=True, help_text="Device Name")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Device Areas"

    def __str__(self):
        return self.name


class RUAreaDiscount(models.Model):
    area = models.OneToOneField(RUArea, on_delete=models.CASCADE, primary_key=True, related_name='ru_discount', help_text="Select Area for Device")
    discount_rate = models.DecimalField(_("Discount Rate"), max_digits=5, decimal_places=2, help_text='in percentage (%)', default=0)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Area based Discounts"

    def __str__(self):
        return self.area.name + ' | ' + str(self.discount_rate) + '%'


class RUDevice(models.Model):
    name = models.CharField(_("Device Name"), max_length=100, blank=False, null=False, help_text="Device Name", default='')
    image = FilerImageField(null=True, blank=True, related_name="remote_unit_image", on_delete=models.CASCADE, help_text="Upload/Select image for this remote unit")
    area = models.ForeignKey(RUArea, on_delete=models.CASCADE, null=True, blank=True, related_name='ru_area', help_text="Select Area for Device")
    zone = models.ForeignKey(RUZone, on_delete=models.CASCADE, null=True, blank=True, related_name='ru_zone', help_text="Select Zone for Device")
    discount_group = models.ForeignKey(DiscountGroup, on_delete=models.CASCADE, null=True, blank=True, related_name='ru_discount_group', help_text="Select Discount Group")
    address = models.TextField(_("Address"), blank=True)
    yt_link = models.CharField(_("Youtube Video Link"), max_length=150, blank=True, default='')
    tag = models.CharField(_("Tag"), max_length=250, blank=True, null=True, help_text="Device Tag ID")
    last_active = models.DateTimeField(_("Last Active"), auto_now=False, auto_now_add=False, help_text="last recorded active time", blank=True, null=True)
    lat = models.FloatField(_("Latitude"), blank=True, default=0.0)
    lon = models.FloatField(_("Longitude"), blank=True, default=0.0)
    loc = models.PointField(_("Location"), srid=4326, geography=True, dim=2, null=True, blank=True, editable=False)
    ping_timeout = models.PositiveIntegerField(_("Ping Timeout"), default=20)
    hide_from_site = models.BooleanField(_("Make Private"), default=False, help_text="Hide from public website's listing page")
    disabled = models.BooleanField(_("Disable Device"), default=False)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "RU Devices"

    def __str__(self):
        return self.name

    def magazine_count(self):
        return Magazine.objects.filter(device=self).count()

    def total_products(self):
        magazines = Magazine.objects.filter(device=self)
        count = 0
        for m in magazines:
            count += m.product_count()
        return count

    def total_available(self):
        magazines = Magazine.objects.filter(device=self)
        count = 0
        for m in magazines:
            count += m.available_count()
        return count

    def expired_products_count(self):
        magazines = Magazine.objects.filter(device=self)
        count = 0
        for m in magazines:
            count += m.expired_products_count()
        return count

    def active(self):
        if self.last_active is None:
            return False
        if (now() - self.last_active).total_seconds() >= self.ping_timeout:
            return False

        return True
    active.boolean = True

    def image_preview(self):
        return image_component(self.image)
    image_preview.short_description = 'Image'


class Magazine(models.Model):
    device = models.ForeignKey(RUDevice, on_delete=models.CASCADE, null=False, blank=False, related_name='ru_magazine', help_text="Select Device for magazine")
    magazine_number = models.PositiveIntegerField(_("Magazine Number"), default=0, help_text='Magazine number for selected Remote Unit')
    product_type = models.ForeignKey(ProductType, on_delete=models.CASCADE, null=False, blank=False, related_name='magazine_product_type', help_text="Select product type")
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Magazines"
        unique_together = [['device', 'magazine_number']]

    def __str__(self):
        return self.device.name + ' | ' + str(self.magazine_number)

    def product_count(self):
        return MagazineProduct.objects.filter(magazine=self).count()

    def available_count(self):
        hold_count = 0
        valid_objs = CartItem.objects.filter(invoice__ru_tag=self.device.tag,dispensed=False, mag_no=self.magazine_number)
        if valid_objs.exists():
            paid_obj_ids = [obj.id for obj in valid_objs if obj.invoice.is_paid() and obj.invoice.dispensable()]
            valid_objs = valid_objs.filter(id__in=paid_obj_ids)
            if valid_objs.exists():
                hold_count = valid_objs.aggregate(tcount=Sum('quantity'))['tcount']
        adjusted_count = self.product_count() - hold_count
        return adjusted_count

    def expired_products_count(self):
        expired_products = [ p for p in MagazineProduct.objects.filter(magazine=self) if not p.valid() ]
        return len(expired_products)

    def above_threshold(self):
        platform_config = get_config()
        threshold_value = platform_config.ru_magazine_threshold if platform_config is not None else settings.RU_MAGAZINE_THRESHOLD
        if self.product_count() <= threshold_value:
            return False
        return True
    above_threshold.boolean = True


class MagazineProduct(models.Model):
    magazine = models.ForeignKey(Magazine, on_delete=models.CASCADE, null=False, blank=False, related_name='magazine_product', help_text="Select magazine for product")
    sku = models.CharField(_("SKU"), max_length=100, blank=True, help_text="Store ID", default='')
    valid_until = models.DateTimeField(_("Validity Date"), auto_now=False, auto_now_add=False, default=now)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        sku_id = self.sku if self.sku is not None or not self.sku.strip() == '' else str(self.id)
        return sku_id + ' | ' + self.magazine.__str__()

    def valid(self):
        return True if self.valid_until > now() else False
    valid.boolean = True