from django.db.models import AutoField
from django.forms import ModelForm, CharField, IntegerField, Form
from django.conf import settings

import sys

from store.models import Product, SingleProduct, ProductType
from .models import MagazineProduct, Magazine, RUAreaDiscount


class MagazineForm(ModelForm):
    class Meta:
        model = Magazine
        fields = '__all__'

    def clean(self):
        if self.instance is not None:
            if MagazineProduct.objects.filter(magazine=self.instance).exists():
                msg = "Can't alter product type while products are remaining in magazine!"
                self._errors['product_type'] = self.error_class([msg])
                del self.cleaned_data['product_type']


class RUAreaDiscountForm(ModelForm):
    class Meta:
        model = RUAreaDiscount
        fields = '__all__'

    def clean(self):
        discount_rate = self.cleaned_data.get('discount_rate')
        if discount_rate < 0:
            msg = "Must enter positive area discount rate!"
            self._errors['discount_rate'] = self.error_class([msg])
            del self.cleaned_data['discount_rate']
        elif discount_rate == 0:
            msg = "Must enter non-zero area discount rate! If you want no discounts for this area, delete the area discount record."
            self._errors['discount_rate'] = self.error_class([msg])
            del self.cleaned_data['discount_rate']


def copy_model_instance(obj):
    initial = dict([(f.name, getattr(obj, f.name)) for f in obj._meta.fields if not isinstance(f, AutoField) and not f in obj._meta.parents.values()])
    return obj.__class__(**initial)


class MagazineProductForm(ModelForm):
    product_sku = CharField()
    quantity = IntegerField()
    class Meta:
        model = MagazineProduct
        exclude = ('sku', 'product_type', 'valid_until')

    def clean(self):
        product_sku = self.cleaned_data.get('product_sku')
        quantity = self.cleaned_data.get('quantity')
        magazine = self.cleaned_data.get('magazine')

        if not Product.objects.filter(sku=product_sku).exists():
            msg = "Must enter valid sku!"
            self._errors['product_sku'] = self.error_class([msg])
            del self.cleaned_data['product_sku']

        product_type = Product.objects.filter(sku=product_sku).first().product_type
        magazine_product_type = magazine.product_type
        if not product_type == magazine_product_type:
            msg = "product_type for sku doesn\'t match with the selected magazine!"
            self._errors['product_sku'] = self.error_class([msg])
            del self.cleaned_data['product_sku']

        if quantity == 0:
            msg = "Must enter quantity greater than zero"
            self._errors['quantity'] = self.error_class([msg])
            del self.cleaned_data['quantity']

        inventory_count = SingleProduct.objects.filter(product__sku=product_sku).count()

        if quantity > inventory_count:
            msg = "Entered quantity is greater than inventory. Current inventory for {} is {}".format(product_sku, inventory_count)
            self._errors['quantity'] = self.error_class([msg])
            del self.cleaned_data['quantity']

        inv_type = product_type.inventory_type
        max_capacity = inv_type.max_capacity
        try:
            max_capacity = int(max_capacity)
        except:
            max_capacity = settings.RU_MAGAZINE_LENGTH

        if quantity + magazine.product_count() > max_capacity:
            msg = "Can't enter {} products in magazine. Remaining capacity is {}".format(quantity, max_capacity - magazine.product_count())
            self._errors['quantity'] = self.error_class([msg])
            del self.cleaned_data['quantity']

        return self.cleaned_data

    def save(self, commit=True, *args, **kwargs):
        m = super(MagazineProductForm, self).save(commit=False, *args, **kwargs)
        m_new = copy_model_instance(m)
        pr_sku = self.cleaned_data.get('product_sku')
        pr_qt = self.cleaned_data.get('quantity')
        magazine = self.cleaned_data.get('magazine')

        targeted_product = Product.objects.filter(sku=pr_sku).first()
        m_new.sku = targeted_product.sku
        m_new.magazine = magazine
        m_new.valid_until = targeted_product.valid_until
        m_new.save()

        # bulk_create other
        if pr_qt > 1:
            products_to_create = [MagazineProduct(sku=targeted_product.sku, magazine=magazine, valid_until=targeted_product.valid_until) for i in range(pr_qt-1)]
            MagazineProduct.objects.bulk_create(products_to_create)

        # remove entries from inventory/store
        store_entries = SingleProduct.objects.filter(product=targeted_product).order_by('-id')[:pr_qt]
        for entry in store_entries:
            entry.delete()
        targeted_product.count = targeted_product.count - pr_qt
        targeted_product.save()

        return m_new