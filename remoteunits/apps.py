from django.apps import AppConfig


class RemoteunitsConfig(AppConfig):
    name = 'remoteunits'
    verbose_name = 'Remote Units'

    def ready(self):
        import remoteunits.signals