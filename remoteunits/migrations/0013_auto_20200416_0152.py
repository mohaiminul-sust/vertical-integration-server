# Generated by Django 3.0.5 on 2020-04-15 19:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0014_auto_20200415_2114'),
        ('remoteunits', '0012_remove_rudevice_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='magazineproduct',
            name='product_type',
        ),
        migrations.AddField(
            model_name='magazine',
            name='product_type',
            field=models.ForeignKey(blank=True, help_text='Select product type', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='magazine_product_type', to='store.ProductType'),
        ),
    ]
