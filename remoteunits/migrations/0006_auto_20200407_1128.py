# Generated by Django 3.0.5 on 2020-04-07 05:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0009_singleproduct_status'),
        ('remoteunits', '0005_rudevice_ping_timeout'),
    ]

    operations = [
        migrations.AlterField(
            model_name='megazine',
            name='products',
            field=models.ManyToManyField(blank=True, null=True, to='store.SingleProduct', verbose_name='Products'),
        ),
    ]
