# Generated by Django 3.0.5 on 2020-04-22 18:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('remoteunits', '0016_ruarea_pr_sku_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ruarea',
            name='name',
            field=models.CharField(help_text='Device Name', max_length=100, verbose_name='Area Name'),
        ),
        migrations.AlterUniqueTogether(
            name='ruarea',
            unique_together={('name', 'pr_sku_code')},
        ),
    ]
