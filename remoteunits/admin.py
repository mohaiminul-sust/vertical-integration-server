from django.contrib import admin
from django.utils.html import format_html

from .models import RUDevice, Magazine, MagazineProduct, RUArea, RUZone, RUAreaDiscount
from .forms import MagazineProductForm, RUAreaDiscountForm, MagazineForm
from .convenience import RemoteUnitIsActiveFilter, RemoteUnitContainsExpiredFilter, MagazineThresholdFilter

from mapwidgets.widgets import GooglePointFieldWidget
from django.contrib.gis.db import models


class RUZoneAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('name', 'created_date', 'updated_date')
    ordering= ('name',)

admin.site.register(RUZone, RUZoneAdmin)


class RUAreaAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('name', 'created_date', 'updated_date')
    ordering= ('name',)

admin.site.register(RUArea, RUAreaAdmin)


class RUAreaDiscountAdmin(admin.ModelAdmin):
    form = RUAreaDiscountForm
    date_hierarchy = 'created_date'
    list_display = ('area', 'discount_rate', 'created_date', 'updated_date')
    list_filter = ('area',)
    search_fields = ('area__name',)
    ordering= ('area__name',)

admin.site.register(RUAreaDiscount, RUAreaDiscountAdmin)


class MagazineInline(admin.TabularInline):
    model = Magazine
    show_change_link = True
    readonly_fields = ('id','total', 'available', 'expired', 'above_threshold')
    can_delete = False
    extra = 0

    def total(self, obj):
        count = obj.product_count()
        if count > 0:
            return count
        else:
            return format_html('<span style="color: red;">{}</span>', count)

    def available(self, obj):
        count = obj.available_count()
        if count > 0:
            return count
        else:
            return format_html('<span style="color: red;">{}</span>', count)

    def expired(self, obj):
        count = obj.expired_products_count()
        if count == 0:
            return count
        else:
            return format_html('<span style="color: red;">{}</span>', count)


class RUDeviceAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }
    date_hierarchy = 'created_date'
    inlines = [MagazineInline,]
    list_display = ('name', 'image_preview', 'magazine_count', 'total', 'available', 'expired', 'tag', 'active', 'last_active', 'created_date')
    list_filter = (RemoteUnitContainsExpiredFilter, RemoteUnitIsActiveFilter, 'zone', 'area', 'discount_group')
    ordering= ('-created_date',)

    # def contains_expired(self, obj):
    #     return True if obj.expired_products_count() > 0 else False

    def expired(self, obj):
        return obj.expired_products_count()

    def total(self, obj):
        return obj.total_products()

    def available(self, obj):
        return obj.total_available()

admin.site.register(RUDevice, RUDeviceAdmin)


class MagazineAdmin(admin.ModelAdmin):
    form = MagazineForm
    date_hierarchy = 'created_date'
    list_display = ('id', 'device', 'magazine_number', 'product_type', 'total', 'available', 'expired', 'above_threshold', 'created_date')
    list_filter= (MagazineThresholdFilter, RemoteUnitContainsExpiredFilter, 'device', 'product_type')
    ordering= ('-created_date',)

    def total(self, obj):
        count = obj.product_count()
        if count > 0:
            return count
        else:
            return format_html('<span style="color: red;">{}</span>', count)

    def available(self, obj):
        count = obj.available_count()
        if count > 0:
            return count
        else:
            return format_html('<span style="color: red;">{}</span>', count)

    def expired(self, obj):
        count = obj.expired_products_count()
        if count == 0:
            return count
        else:
            return format_html('<span style="color: red;">{}</span>', count)

admin.site.register(Magazine, MagazineAdmin)


class MagazineProductAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_display = ('id', 'sku', 'magazine', 'valid', 'valid_until', 'created_date')
    list_filter = ('magazine__device', 'magazine')
    form = MagazineProductForm
    ordering= ('-created_date',)

admin.site.register(MagazineProduct, MagazineProductAdmin)