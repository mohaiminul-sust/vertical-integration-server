from django.urls import path, include
from django.conf.urls import url

from .apiviews import RemoteUnitStatusAPIView, RemoteUnitsListAPIView, RemoteUnitRetrieveAPIView


urlpatterns = [
    url(r'^remoteunits/status/$', RemoteUnitStatusAPIView.as_view(), name='ru_status_view'),
    url(r'^spa/remote-units/single/$', RemoteUnitRetrieveAPIView.as_view(), name='ru_single_view'),
    url(r'^spa/remote-units/$', RemoteUnitsListAPIView.as_view(), name='ru_list_view'),
]