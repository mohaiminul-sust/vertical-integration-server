from django.contrib import admin
from django.http import JsonResponse
from django.utils.crypto import get_random_string
from .models import RUDevice
from users.otp_handlers import send_sms

from django.core.mail import send_mail
from django.template import Context, loader
from django.conf import settings


def get_device_from_tag(tag):
    device_objs = RUDevice.objects.filter(tag=tag)
    if device_objs.exists():
        return device_objs.first()
    else:
        return None


def get_area_from_tag(tag):
    device_obj = get_device_from_tag(tag)
    if device_obj is not None:
        return device_obj.area
    else:
        return None


def send_dispense_message(cart_item):
    user = cart_item.user
    inv = cart_item.invoice
    if inv.dispensed():
        if user.can_receive_mail():
            my_template = loader.get_template('account/email/dispense_success_message.txt')
            my_context = Context({
                'invoice_id': inv.id,
                'ru_name': inv.ru_name,
                'ru_address': inv.ru_address,
                'ru_area': inv.ru_area,
                'ru_zone': inv.ru_zone
            })
            dispense_success_message = my_template.render(my_context.flatten())
            send_mail("Products Dispensed | Vertical Innovations", dispense_success_message, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=True)
        if user.can_receive_sms():
            ack_str = 'Your products for invoice {} have dispensed. Please collect them from {} at {} in {}, {}'.format(inv.id, inv.ru_name, inv.ru_address, inv.ru_area, inv.ru_zone)
            send_sms(user.sms_number(), ack_str)


def get_new_ru_tag():
    new_id = get_random_string(length=12)
    if RUDevice.objects.filter(tag=new_id).exists():
        return get_new_ru_tag()
    return new_id


def ru_response(dispense_mag_no, msg=0, status=200, tran_id='', disabled=0, quantity=0):
    return JsonResponse(
        {
            "mag": dispense_mag_no,
            "qty": quantity,
            "info": msg,
            "id": str(tran_id),
            "disabled": disabled
        },
        status=status
    )


class RemoteUnitIsActiveFilter(admin.SimpleListFilter):
    title = 'Unit Activity'
    parameter_name = 'ru_is_active'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Active'),
            ('No', 'Idle'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            active_ids = [unit.id for unit in queryset.all() if unit.active()]
            return queryset.filter(id__in=active_ids)
        elif value == 'No':
            idle_ids = [unit.id for unit in queryset.all() if not unit.active()]
            return queryset.filter(id__in=idle_ids)
        return queryset


class RemoteUnitContainsExpiredFilter(admin.SimpleListFilter):
    title = 'Expiry'
    parameter_name = 'ru_contains_expired'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Contains Expired Products'),
            ('No', 'No Expired Products'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            has_expired_ids = [unit.id for unit in queryset.all() if unit.expired_products_count() > 0]
            return queryset.filter(id__in=has_expired_ids)
        elif value == 'No':
            no_expired_ids = [unit.id for unit in queryset.all() if unit.expired_products_count() == 0]
            return queryset.filter(id__in=no_expired_ids)
        return queryset


class MagazineThresholdFilter(admin.SimpleListFilter):
    title = 'Threshold Warning'
    parameter_name = 'magazine_threshold_warning'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Above Threshold'),
            ('No', 'Reached Threshold'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            above_ids = [mag.id for mag in queryset.all() if mag.above_threshold()]
            return queryset.filter(id__in=above_ids)
        elif value == 'No':
            reached_ids = [mag.id for mag in queryset.all() if not mag.above_threshold()]
            return queryset.filter(id__in=reached_ids)
        return queryset


# immensely proud of this :D
def ls_intersection(list_a, list_b):
    return [ e for e in list_a if e in list_b ]


# immensely proud of this too ;)
def intersect_2d_array_by_subarray(d_arr):
    cmpr= []
    for index, ls in enumerate(d_arr):
        # print(index, file=sys.stderr)
        # print(ls, file=sys.stderr)
        if index == 0:
            cmpr = ls
        else:
            cmpr = ls_intersection(cmpr, ls)
    # print(cmpr, file=sys.stderr)
    return cmpr