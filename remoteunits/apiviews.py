from django.utils.timezone import now
from django.shortcuts import get_object_or_404
from django.core import serializers
from django.http import JsonResponse
from rest_framework import generics, status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable
from rest_framework.decorators import api_view
from decimal import Decimal

from transaction.models import Transaction, CartItem
from users.models import RFIDInfo, RFIDItem
from store.models import ProductType
from store.serializers import ProductTypeSerializer

from .models import RUDevice, Magazine, MagazineProduct
from .serializers import MagazineSerializer, RUDeviceSerializer
from .convenience import ru_response, intersect_2d_array_by_subarray, send_dispense_message

import sys
import json
from django.contrib.gis.geos import Point
from django.contrib.gis.db.models.functions import GeometryDistance
from django.db.models import Sum


class RemoteUnitRetrieveAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        if 'tag' not in request.query_params:
            raise NotAcceptable('Must have tag in param!')
        tag = request.query_params['tag']
        device = RUDevice.objects.filter(tag=tag).first()
        device_data = RUDeviceSerializer(device, many=False).data
        mags = Magazine.objects.filter(device=device)
        product_types = []
        total_count = 0
        for mag in mags:
            product_type = mag.product_type
            product_type_data = ProductTypeSerializer(product_type, many=False).data
            product_type_data['product_count'] = mag.available_count()
            total_count += mag.available_count()
            # product_type_data['machine_goods_count'] = mag.product_count()
            product_type_data['mag_no'] = mag.magazine_number
            product_types.append(product_type_data)

        device_data['types'] = product_types
        device_data['total_products'] = total_count

        return Response({
            "device": device_data
        })


class RemoteUnitsListAPIView(generics.ListAPIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        # print(request.query_params, file=sys.stderr)
        devices = RUDevice.objects.filter(disabled=False, hide_from_site=False)
        device_ids = [ entry.id for entry in devices if entry.active() ]
        devices = RUDevice.objects.filter(id__in=device_ids)

        # Filters
        if 'zones' in request.query_params:
            zone_ids = request.query_params['zones']
            zone_ids = json.loads(zone_ids)
            if len(zone_ids) > 0:
                devices = devices.filter(zone__in=zone_ids)

        if 'areas' in request.query_params:
            area_ids = request.query_params['areas']
            area_ids = json.loads(area_ids)
            if len(area_ids) > 0:
                devices = devices.filter(area__in=area_ids)

        if 'types' in request.query_params:
            type_ids = request.query_params['types']
            type_ids = json.loads(type_ids)
            if len(type_ids) > 0:
                incl_devices = []
                for tid in type_ids:
                    t_devices = []
                    for device in devices:
                        magazines = Magazine.objects.filter(device=device, product_type__inventory_type__id=tid)
                        if magazines.exists():
                            t_devices.append(device.id)
                    incl_devices.append(t_devices)
                common_devices = intersect_2d_array_by_subarray(incl_devices)
                devices = devices.filter(id__in=common_devices)

        if 'companies' in request.query_params:
            company_ids = request.query_params['companies']
            company_ids = json.loads(company_ids)
            if len(company_ids) > 0:
                incl_devices = []
                for cid in company_ids:
                    c_devices = []
                    for device in devices:
                        magazines = Magazine.objects.filter(device=device, product_type__name__id=cid)
                        if magazines.exists():
                            c_devices.append(device.id)
                    incl_devices.append(c_devices)
                # print(incl_devices, file=sys.stderr)
                common_devices = intersect_2d_array_by_subarray(incl_devices)
                devices = devices.filter(id__in=common_devices)

        if 'classes' in request.query_params:
            class_ids = request.query_params['classes']
            class_ids = json.loads(class_ids)
            if len(class_ids) > 0:
                incl_devices = []
                for cid in class_ids:
                    c_devices = []
                    for device in devices:
                        magazines = Magazine.objects.filter(device=device, product_type__product_class__id=cid)
                        if magazines.exists():
                            c_devices.append(device.id)
                    incl_devices.append(c_devices)
                # print(incl_devices, file=sys.stderr)
                common_devices = intersect_2d_array_by_subarray(incl_devices)
                devices = devices.filter(id__in=common_devices)

        if 'sizes' in request.query_params:
            size_ids = request.query_params['sizes']
            size_ids = json.loads(size_ids)
            if len(size_ids) > 0:
                incl_devices = []
                for sid in size_ids:
                    s_devices = []
                    for device in devices:
                        magazines = Magazine.objects.filter(device=device, product_type__size__id=sid)
                        if magazines.exists():
                            s_devices.append(device.id)
                    incl_devices.append(s_devices)
                # print(incl_devices, file=sys.stderr)
                common_devices = intersect_2d_array_by_subarray(incl_devices)
                devices = devices.filter(id__in=common_devices)

        # sort with location
        if 'lat' in request.query_params:
            if 'lon' in request.query_params:
                if not request.query_params['lat'].strip() == '' and not request.query_params['lon'].strip() == '':
                    try:
                        user_lat = float(request.query_params['lon'])
                        user_lon = float(request.query_params['lat'])
                        user_location = Point(user_lon, user_lat, srid=4326)
                        devices = devices.order_by(GeometryDistance("loc", user_location))
                    except:
                        pass

        # Serialize Payload
        devices_data = []
        for device in devices:
            device_data = RUDeviceSerializer(device, many=False).data
            mags = Magazine.objects.filter(device=device.id)
            product_types = []
            for mag in mags:
                product_type = mag.product_type
                product_type_data = ProductTypeSerializer(product_type, many=False).data
                product_type_data['product_count'] = mag.available_count()
                product_types.append(product_type_data)

            # user_location = None
            # if 'lat' in request.query_params:
            #     if 'lon' in request.query_params:
            #         if not request.query_params['lat'].strip() == '' and not request.query_params['lon'].strip() == '':
            #             user_lat = float(request.query_params['lon'])
            #             user_lon = float(request.query_params['lat'])
            #             user_location = Point(user_lon, user_lat, srid=4326)

            # device_data['distance'] = round(device.loc.distance(user_location) * 100, 2) if user_location is not None else None
            device_data['types'] = product_types
            devices_data.append(device_data)

        return Response({
            "devices": devices_data
        })


class RemoteUnitStatusAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        if 'ru_tag' not in request.query_params:
            # raise NotAcceptable('must have ru_tag query parameter!')
            return ru_response(0, 0, 406)

        ru_tag = request.query_params['ru_tag']
        if ru_tag.strip() == '':
            # raise NotAcceptable('ru_tag can\'t be empty!')
            return ru_response(0, 0, 406)

        remote_units = RUDevice.objects.filter(tag=ru_tag)
        if not remote_units.exists():
            # raise NotFound('remote unit not found for given tag.')
            return ru_response(0, 0, 404)

        remote_unit = remote_units.first()

        # register ping in server
        remote_unit.last_active = now()
        remote_unit.save()

        # START - Transaction update hook for dispense
        if 'tran_id' in request.query_params:
            tran_id = request.query_params['tran_id'].strip()
            if not tran_id == '':
                cart_items = CartItem.objects.filter(id=tran_id, dispensed=False)
                if cart_items.exists():
                    cart_item = cart_items.first()
                    cart_item.dispensed = True
                    cart_item.dispense_date = now()
                    cart_item.save()
                    # commit rfid balance updates for rfid transaction
                    transactions = Transaction.objects.filter(invoice=cart_item.invoice, status="PAID", dispensed=True)
                    if transactions.exists():
                        transaction = transactions.first()
                        if transaction.gateway_type == 1:
                            rfid = RFIDInfo.objects.filter(tag=transaction.rfid_tag).first()
                            rfid.last_used = now()
                            rfid.save()
                            magazine = Magazine.objects.filter(device__tag=transaction.ru_tag, magazine_number=cart_item.mag_no).first()
                            rfid_items = RFIDItem.objects.filter(product_type=magazine.product_type, rfid=rfid, quantity__gte=cart_item.quantity, expire_date__gte=now()).order_by('expire_date')
                            if rfid_items.exists():
                                rfid_item = rfid_items.first()
                                rfid_item.quantity -= cart_item.quantity
                                rfid_item.save()
                        # remove dispensed product from magazine
                        mag_prods = MagazineProduct.objects.filter(magazine__device__tag=transaction.ru_tag, magazine__magazine_number=cart_item.mag_no)
                        if mag_prods.exists():
                            prods=mag_prods.order_by('created_date')[:cart_item.quantity]
                            for prod in prods:
                                prod.delete()

                    send_dispense_message(cart_item)
        # END - Transaction update hook for dispense

        # START - Auto dispense for Web SSL, APP/USSD
        dispense_magazine = 0
        tran_id = ''
        quantity = 0
        cart_transactions = Transaction.objects.filter(ru_tag=ru_tag, dispensed=True, status="PAID")
        valid_tran_ids = [tran.id for tran in cart_transactions if tran.dispensable_items().exists() and tran.invoice.dispensable()]
        cart_transactions = cart_transactions.filter(id__in=valid_tran_ids)
        if cart_transactions.exists():
            cart_transaction = cart_transactions.order_by('tran_date').first()
            cart_item = cart_transaction.dispensable_items().order_by('created_date').first()
            dispense_magazine = cart_item.mag_no
            tran_id = cart_item.id
            quantity = cart_item.quantity
        # END - Auto dispense for Web SSL, APP/USSD

        return ru_response(dispense_magazine, 1, 200, tran_id, 1 if remote_unit.disabled else 0, quantity=quantity)