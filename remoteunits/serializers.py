from rest_framework import serializers
from django.conf import settings
from users.convenience import filer_url_for

from .models import Magazine, MagazineProduct, RUDevice, RUArea, RUZone, RUAreaDiscount
from users.serializers import DiscountGroupSerializer


class RUZoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = RUZone
        fields = ('id', 'name',)


class RUAreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = RUArea
        fields = ('id', 'name',)


class RUAreaDiscountSerializer(serializers.ModelSerializer):
    class Meta:
        model = RUAreaDiscount
        fields = ('discount_rate',)


class RUDeviceSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    total_products = serializers.SerializerMethodField()
    active = serializers.SerializerMethodField()
    area_name = serializers.CharField(source="area.name")
    zone_name = serializers.CharField(source="zone.name")
    discount_group = DiscountGroupSerializer(read_only=True)
    area_discount = RUAreaDiscountSerializer(source="area.ru_discount", read_only=True)
    class Meta:
        model = RUDevice
        # fields = '__all__'
        exclude = ('ping_timeout', 'last_active', 'created_date', 'updated_date',)

    def get_image(self, obj):
        return filer_url_for(obj.image)

    def get_total_products(self, obj):
        return obj.total_available()

    def get_active(self, obj):
        return obj.active()


class MagazineSerializer(serializers.ModelSerializer):
    count = serializers.SerializerMethodField()
    class Meta:
        model = Magazine
        exclude = ('id', 'device', 'created_date', 'updated_date',)

    def get_count(self, obj):
        return obj.product_count()