from django.db import models
from django.utils.translation import gettext as _
from ckeditor.fields import RichTextField

# Create your models here.
class PlatformConfiguration(models.Model):
    name = models.CharField(_("Configuration Name"), max_length=50)
    ru_magazine_threshold = models.PositiveIntegerField(_("RU Megazine Threshold"), default=5, help_text="Remote Unit's megazine warning threshold for product count")
    product_sku_tail_length = models.PositiveIntegerField(_("Product SKU Tail Characters"), default=4, help_text="Maximum length of auto-generated tail for product SKU")
    otp_digits_length = models.PositiveIntegerField(_("OTP Digit's Length"), default=6, help_text="Maximum length of auto-generated OTP Code")
    totp_timeout= models.PositiveIntegerField(_("OTP Response Time (Seconds)"), default=180, help_text="Maximum Time (In Seconds) for valid response for OTP Messages")
    cart_timeout = models.PositiveIntegerField(_("Cart Holdout Time (Hours)"), default=12, help_text="Maximum Time (In Hours) for validity of purchased items holdout on certain RU")
    terms_of_use = RichTextField(blank=True)
    privacy_policy = RichTextField(blank=True)
    active = models.BooleanField(_("Active"), default=False)
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Platform Configurations"

    def __str__(self):
        return self.name