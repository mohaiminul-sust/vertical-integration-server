from django.apps import AppConfig


class PlatformManagementConfig(AppConfig):
    name = 'platform_management'
    verbose_name = 'Platform Management'

    def ready(self):
        import platform_management.signals