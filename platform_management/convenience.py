from .models import PlatformConfiguration


def get_config():
    all_configs = PlatformConfiguration.objects.all()
    active_configs = all_configs.filter(active=True)

    if all_configs.count() == 0:
        return None

    if all_configs.count() > 1:
        if active_configs.count() > 0:
            return active_configs.first()
        else:
            all_configs.first()
    return all_configs.first()