from django.contrib import admin
from .models import PlatformConfiguration
# Register your models here.

class PlatformConfigurationAdmin(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        if PlatformConfiguration.objects.all().count() > 1:
            return True
        return False

admin.site.register(PlatformConfiguration, PlatformConfigurationAdmin)