# Vertical Innovations BaaS

* Vertical Innovations Server API
* Custom Admin Dashboard & Admin Controls

## Tech Stack
* Django - 3.0.5
* Django REST Framework - 3.11.0
* REST Auth - 0.9.5
* Pillow (Image Manipulation Library) - 7.1.0
* Nexmo (Messaging Service Provider) - 2.4.0
* AnyMail (Mailgun : Mail Service Provider) - 7.1.0
* Ckeditor - 5.9.0
* Filer - 2.2.0
* Admin Interface - 0.12.2
* Map Widgets - 0.3.0
* CopreAPI Docs - 2.3.3
* Gunicorn - 20.0.4
* Psychopg2 (Postgres Connector) - 2.8.4
* Sentry - 0.14.4
* PostGIS

check full requirements list [here](requirements.txt)...

## Getting Started
Setup project environment with [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/install.html) and [pip](https://pip.pypa.io).

```bash
$ mkvirtualenv viserver
$ workon viserver

# clone the repo, navigate to the project and then ...
$ pip install -r requirements.txt

# Configure Postgresql DB according to project settings and run the command below
$ ./manage.py migrate

# Just run the server
$ ./manage.py runserver 0.0.0.0:8000

# Run tests
$ ./manage.py test
```

For API documentation visit [here](https://documenter.getpostman.com/view/198290/SzzkcxTA?version=latest)

## Features
* Fully documented API with CoreAPI and Markdown
* Custom Auth System with password reset and customized user accounts and groups
* 2 Factor Authentication (TOTP) using Nexmo Client API
* Entity files are managable with custom file system & file permissions for users
* Fully customized entity mangement on Store & Device modules
* 2D Location data storage with postgis extension for postgresql and location based device query APIs
* Custom wallet(cart) and Transaction module
* Favicon management in Admin